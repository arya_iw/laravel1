$(document).on('click', '.pagination a', function (event) {
    event.preventDefault();
    let page = $(this).attr('href').split('page=')[1];
    $('#hidden_page').val(page);
    let data_sort = $('#hidden_data_sort').val();
    let data_sort_type = $('#hidden_data_sort_type').val();
    let query = $('.search').val()
    fetch_book_data(page, data_sort_type, data_sort, query);
});
function fetch_book_data(page, sort_type, sort_by, query) {
    $.ajax({
        url: `/fetch_book_data?page=${page}&sortby=${sort_by}&sorttype=${sort_type}&query=${query}`,
        success: function (data) {
            if (data.length == 59) {
                $('.card-book').html('');
                $('.ALERT').css('display', 'block')
            } else {
                $('.ALERT').css('display', 'none')
                $('.card-book').html('');
                $('.card-book').html(data);
            }
        }
    })
}
$(document).on('keyup', '.search', function () {
    let query = $(this).val()
    let data_sort = $('#hidden_data_sort').val();
    let data_sort_type = $('#hidden_data_sort_type').val();
    let page = $('#hidden_page').val(1);
    fetch_book_data(page, data_sort_type, data_sort, query);
})
$(document).on('click', '.sorting', function () {
    let data_sort = $(this).data('sort');
    let data_sort_type = $(this).data('sort_type');
    let data_reverse = '';
    if (data_sort_type == 'asc') {
        $(this).data('sort_type', 'desc');
        data_reverse = 'desc';
        $(`#${data_sort}_icon`).removeClass().addClass('fas fa-sort-amount-up');
    }
    if (data_sort_type == 'desc') {
        $(this).data('sort_type', 'asc');
        data_reverse = 'asc';
        $(`#${data_sort}_icon`).removeClass().addClass('fas fa-sort-amount-down');

    }
    $('#hidden_data_sort').val(data_sort);
    $('#hidden_data_sort_type').val(data_reverse);
    let page = $('#hidden_page').val();
    let query = $('.search').val();
    fetch_book_data(page, data_reverse, data_sort, query);
});
