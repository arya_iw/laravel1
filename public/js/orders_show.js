$('select[name="province_destination"]').on('change', function () {
    $('select[name="couriers_destination"] option').prop('selected', function () {
        return this.defaultSelected;
    });
    let provinceId = $(this).val();
    if (provinceId) {
        $.ajax({
            url: `/province/${provinceId}/cities`,
            type: "GET",
            dataType: "json",
            success: function (data) {
                $('select[name="city_destination"]').empty();
                $.each(data, function (key, value) {
                    $('select[name="city_destination"]').append(`<option value="${key}">${value}</option>`)
                })
            }
        })
    }
});
$('select[name="couriers_destination"]').on('change', function () {
    $('input[name="value"]').val('');
    let city_origin = $('input[name="city_origin"]').val();
    let city_destination = $('select[name="city_destination"]').val()
    let courier = $(this).val();
    getOngkosKirim(city_origin, city_destination, courier);
});
function getOngkosKirim(city_origin, city_destination, courier) {
    $.ajax({
        url: `/ongkir`,
        type: "GET",
        data: {
            city_origin: city_origin,
            city_destination: city_destination,
            courier: courier
        },
        success: function (data) {
            $('select[name="service"]').empty();
            $('select[name="service"]').append('<option value="0">Service</option>')
            $.each(data, function (index1, array1) {
                $.each(array1["costs"], function (index2, array2) {
                    $.each(array2.cost, function (index3, array3) {
                        $('select[name="service"]').append(`<option value="${array3.value}">${array2.service}(${array2.description})</option>`)
                    })
                })
            })
        }
    })
};
$('select[name="service"]').click(function () {
    let ongkir = $(this).val()
    $('input[name="value"]').val(ongkir);
});
$book_price = $('#book-price').data('price')
$('input[name="quantity"]').val($('#quantity').val());
$(document).on('input', '#quantity', function () {
    $price = $(this).val() * $book_price;
    $('#book-price').text(`Rp.${$price}`);
    $('input[name="quantity"]').val($(this).val());
})
