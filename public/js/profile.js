function editProfile(id) {
    $('input[name="_method"]').val('PUT');
    $('#modal_form form')[0].reset();
    $.ajax({
        url: `/profile/${id}/edit`,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            $('#modal_form').modal('show');
            $('.modal-title').text('Update Profile');
            $('#id').val(data.id);
            $('#name').val(data.name);
            $('#email').val(data.email);
            $('#username').val(data.username);
            $('#phone').val(data.phone);
            $('#address').val(data.address);
            $('.old_avatar').val(data.avatar);
        }, error: function () {
            alert("Nothing Data");
        }
    })
}

// $(function () {
//     $('#modal_form form').on('submit', function (e) {
//         let id = $('#id').val();
//         let formData = new FormData($('.form-data')[0]);
//         e.preventDefault();
//         $.ajax({
//             url: `/profile/${id}`,
//             method: "post",
//             data: formData,
//             contentType: false,
//             cache: false,
//             processData: false,
//             success: function (data) {
//                 $('#modal_form').modal('hide');
//                 location.reload();
//             },
//             error: function () {
//                 alert('Oops Something errors');
//             }
//         })
//     })
// });

$(function () {
    if ($('#modal_form form').length > 0) {
        $('#modal_form form').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 5,
                    maxlength: 100
                },
                current_password: {
                    required: function () {
                        return $('#password').val() != '';
                    }
                },
                password: {
                    required: function () {
                        return $('#current_password').val() != '';
                    }
                },
                phone: {
                    required: true,
                    minlength: 10,
                    maxlength: 12,
                    digits: true
                },
                address: {
                    required: true,
                    minlength: 20,
                    maxlength: 200
                }
            },
            messages: {
                name: {
                    required: "Enter name detail",
                    minlength: "Name must be minimum 5 character long",
                    maxlength: "Name must be minimum 100 character long"
                },
                current_password: {
                    required: "Enter current password detail"
                },
                password: {
                    required: "Enter password detail"
                },
                phone: {
                    required: "Enter phone number detail",
                    minlength: "Phone number must be minimum 10 character long",
                    maxlength: "Phone number must be minimum 12 character long",
                    digits: "Value must be number"
                },
                address: {
                    required: "Enter address detail",
                    minlength: "Password must be minimum 20 character long",
                    maxlength: "Password must be minimum 200 character long"
                }
            },
            submitHandler: function () {
                let id = $('#id').val();
                let formData = new FormData($('.form-data')[0]);
                $.ajax({
                    url: `/profile/${id}`,
                    method: "post",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $('#modal_form').modal('hide');
                        location.reload();
                    },
                    error: function () {
                        alert('Oops Something errors');
                    }
                })
            }
        })
    }
})
