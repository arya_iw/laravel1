function contactOwner() {
    $.ajax({
        url: `/messages/owner`,
        type: 'GET',
        dataType: 'JSON',
        success: function (data) {
            $('#contact_owner').modal('show');
            $('.body_contact_owner').empty();
            $.each(data, function (index, data) {
                let contact = '';
                contact += `
                <form action="/messages/${data.id}">
                <div class="media shadow-lg bg-white mt-3 p-3">
                    <img src="/storage/${data.avatar}" class="mr-3" alt="..." height="60px" height="60px">
                    <div class="media-body">
                        <p class="mt-0">${data.name}</p>
                        <button type="submit" class="btn btn-primary btn-sm">Chat</button>
                    </div>
                </div>
                </form>
                `
                $('.body_contact_owner').append(contact);
            })
        },
        error: function (response) {
            alert(response);
        }
    })
}