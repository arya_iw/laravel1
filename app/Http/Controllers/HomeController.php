<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Order;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()) {
            $all_users = User::all()->count();
            $all_orders = Order::where('user_id', '=', Auth::user()->id)->count();
            $chat = User::where('id','not like',Auth::user()->id)
            ->where('roles', 'like', '["ADMIN"]')->orWhere('roles', 'like', '["STAFF"]')
            ->get();
            return view('home', compact('all_users', 'all_orders','chat'));
        }
        return view('home');
    }
}
