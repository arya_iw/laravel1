<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use App\Category;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (Gate::allows('manage-categories')) {
                return $next($request);
            }
            abort(403, 'Anda tidak memiliki cukup hak akses');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('keyword');
        if ($keyword) {
            $categories= Category::where('name', 'like', "%$keyword%")->paginate(10);
        } else {
            $categories = Category::paginate(10);
        }
        return view('categories.index', ['categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(), [
            "name" => "required|min:3|max:20",
            "image" => "required"
        ])->validate();
        $name = $request->get('name');
        $category = new Category();
        $category->name = $request->get('name');
        if ($request->file('image')) {
            $image_path = $request->file('image')->store('category_images', 'public');
            $category->image = $image_path;
        }
        $category->created_by = \Auth::user()->id;
        $category->slug = \Str::slug($name, '-');
        $category->save();
        return redirect()->route('categories.create')->with('status', 'Category successfully created')->with('type', 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = Category::findOrFail($id);
        return view('categories.show', ['categories'=>$categories]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::findOrFail($id);
        return view('categories.edit', ['categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categories = Category::findOrFail($id);
        \Validator::make($request->all(), [
            "name" => "required|min:3|max:20",
            "image" => "",
            "slug" => [
            "required",
            Rule::unique("categories")->ignore($categories->slug, "slug")
                ]
        ])->validate();
        $name = $request->get('name');
        $categories->name =$request->get('name');
        $categories->updated_by = \Auth::user()->id;
        $categories->slug = \Str::slug($name, '-');
        if ($request->file('image')) {
            if ($categories->image && file_exists(storage_path('app/public/'.$categories->image))) {
                \Storage::delete('public/'. $categories->image);
            }
            $image = $request->file('image')->store('category_images', 'public');
            $categories->image = $image;
        }
        $categories->save();
        return redirect()->route('categories.edit', [$id])->with('status', 'Category succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = Category::findOrFail($id);
        $categories->deleted_by = \Auth::user()->id;
        $categories->save();
        $categories->delete();
        return redirect()->route('categories.index')->with('status', 'Category succesfully Trashed')->with('type', 'warning');
    }
    public function trash()
    {
        $categories_delete = Category::onlyTrashed()->paginate(10);
        return \view('categories.trash', ['categories_delete'=>$categories_delete]);
    }

    public function restore($id)
    {
        $categories_delete = Category::withTrashed()->findOrFail($id);
        if ($categories_delete->trashed()) {
            $categories_delete->deleted_by = null;
            $categories_delete->save();
            $categories_delete->restore();
        } else {
            return redirect()->route('categories.index')->with('status', 'Category is not in trash')->with('type', 'warning');
        }
        return redirect()->route('categories.index')->with('status', 'Category successfully restored')->with('type', 'success');
    }

    public function deletepermanent($id)
    {
        $check_category = DB::table('book_category')->where('category_id','=',$id)->count();
        $categories_delete = Category::withTrashed()->findOrFail($id);
        if (!$categories_delete->trashed()) {
            return redirect()->route('categories.index')->with('status', 'Can not delete permanent active category')->with('type', 'warning');
        } elseif($check_category > 0){
            return redirect()->route('categories.trash')->with('status', 'cannot delete a category related to the order,book data')->with('type','warning');
        } elseif ($categories_delete->trashed()) {
            if ($categories_delete->image && file_exists(storage_path('app/public/' . $categories_delete->image))) {
                \Storage::delete('public/'.$categories_delete->image);
            }
            $categories_delete->forceDelete();
        }
        return redirect()->route('categories.index')->with('status', 'Category permanently deleted')->with('type', 'danger');
    }

    public function ajaxSearch(Request $request)
    {
        $keyword = $request->get('q');
        $categories = Category::where("name", "like", "%$keyword%")->get();
        return $categories;
    }
}
