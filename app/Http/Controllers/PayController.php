<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Pay;
use App\Order;
use Veritrans_Config;
use Veritrans_Snap;
use Veritrans_Notification;
use Veritrans_Transaction;

class PayController extends Controller
{
    // Custom
    public function __construct(Request $request)
    {
        $this->request = $request;
        // Set midtrans configuration
        Veritrans_Config::$serverKey = config('services.midtrans.serverKey');
        Veritrans_Config::$isProduction = config('services.midtrans.isProduction');
        Veritrans_Config::$isSanitized = config('services.midtrans.isSanitized');
        Veritrans_Config::$is3ds = config('services.midtrans.is3ds');
    }

    public function paySubmit(Request $request)
    {
        $check_book_stock = \App\Order::with('books')->where('id', '=', $request->order_id)->get();

        if ($check_book_stock[0]['books'][0]['stock'] < $check_book_stock[0]['books'][0]['pivot']['quantity']) {
            return "stock";
        }
        $data = DB::table('orders')->where('id', '=', $request->order_id)->value('pay_id');
        if ($data == null) {
            $dataPay = \App\Order::with('books')->with('ship')->where('id', '=', $request->order_id)->get();
            DB::transaction(function () use ($dataPay,$request) {
                $pay = new Pay();
                $pay->name = \Auth::user()->name;
                $pay->email = \Auth::user()->email;
                $payload = [
                'transaction_details' => [
                    'order_id' => $request->order_id,
                    'gross_amount' => $dataPay[0]["total_price"],
                ],
                'customer_details' => [
                    'first_name' => $pay->name,
                    'email' => $pay->email,
                ],
                'item_details' => [
                    [
                        'id' => $dataPay[0]['books'][0]['title'],
                        'price' => $dataPay[0]['books'][0]['price'],
                        'quantity' => $dataPay[0]['books'][0]['pivot']['quantity'] ,
                        'name' => ucwords(str_replace('_', ' ', $dataPay[0]['books'][0]['title']))
                    ],
                    [
                        'id' =>"Ongkir ".$dataPay[0]['ship']['courier'],
                        'price' => $dataPay[0]['ship']['cost'],
                        'quantity' => 1 ,
                        'name' => ucwords(str_replace('_', ' ', "Ongkir ".$dataPay[0]['ship']['courier']))
                    ]
                        ]
                    ];
                $snapToken = Veritrans_Snap::getSnapToken($payload);
                $pay->snap_token = $snapToken;
                $pay->save();
                $orderUpdate = \App\Order::findOrFail($request->order_id);
                $orderUpdate->pay_id = $pay->id;
                $orderUpdate->save();
                $this->response['snap_token'] = $snapToken;
            });
        } else {
            $payId = Pay::findOrFail($data);
            return $payId;
        }
        return response()->json($this->response);
    }

    public function payNotif(Request $request)
    {
        $notif = new Veritrans_Notification();
        DB::transaction(function () use ($notif) {
            $transaction = $notif->transaction_status;
            $type = $notif->payment_type;
            $orderId = $notif->order_id;
            $fraud = $notif->fraud_status;
            $payId = Order::findOrFail($orderId);
            $check_book_stock = \App\Order::with('books')->where('id', '=', $orderId)->get();
            $pay = Pay::findOrfail($payId->pay_id);
            if ($type == 'cstore') {
                $pay->payment_type = $notif->store;
                $pay->save();
            } elseif ($type == 'cimb_clicks') {
                $pay->payment_type = "CIMB Clicks";
                $pay->save();
            } elseif ($type == 'credit_card') {
                $pay->payment_type = "CREDIT CARD ".\Str::upper($notif->bank);
                $pay->save();
            } elseif ($type == 'bank_transfer') {
                $pay->payment_type = "Bank Transfer";
                $pay->save();
            } elseif ($type == 'bca_klikpay') {
                $pay->payment_type = "Klikpay BCA";
                $pay->save();
            } elseif ($type == 'danamon_online') {
                $pay->payment_type = "Danamon Online";
                $pay->save();
            } elseif ($type == 'akulaku') {
                $pay->payment_type = "Akulaku";
                $pay->save();
            }
            if ($transaction == 'capture') {
                // For credit card transaction, we need to check whether transaction is challenge by FDS or not
                if ($type == 'credit_card') {
                    if ($fraud == 'challenge') {
                        // TODO set payment status in merchant's database to 'Challenge by FDS'
                        // TODO merchant should decide whether this transaction is authorized or not in MAP
                        // $donation->addUpdate("Transaction order_id: " . $orderId ." is challenged by FDS");
                        $pay->setPending();
                    } else {
                        // TODO set payment status in merchant's database to 'Success'
                        // $donation->addUpdate("Transaction order_id: " . $orderId ." successfully captured using " . $type);
                        $book = \App\Book::findOrFail($check_book_stock[0]['books'][0]['id']);
                        $book->stock = $check_book_stock[0]['books'][0]['stock'] - $check_book_stock[0]['books'][0]['pivot']['quantity'];
                        $book->save();
                        $pay->setSuccess();
                    }
                }
            } elseif ($transaction == 'settlement') {
                // TODO set payment status in merchant's database to 'Settlement'
                // $donation->addUpdate("Transaction order_id: " . $orderId ." successfully transfered using " . $type);
                $book = \App\Book::findOrFail($check_book_stock[0]['books'][0]['id']);
                $book->stock = $check_book_stock[0]['books'][0]['stock'] - $check_book_stock[0]['books'][0]['pivot']['quantity'];
                $book->save();
                $pay->setSuccess();
            } elseif ($transaction == 'pending') {
                // TODO set payment status in merchant's database to 'Pending'
                // $donation->addUpdate("Waiting customer to finish transaction order_id: " . $orderId . " using " . $type);
                $pay->setPending();
            } elseif ($transaction == 'deny') {
                // TODO set payment status in merchant's database to 'Failed'
                // $donation->addUpdate("Payment using " . $type . " for transaction order_id: " . $orderId . " is Failed.");
                $pay->setFailed();
                $payId->status = "CANCEL";
                $payId->save();
            } elseif ($transaction == 'expire') {
                // TODO set payment status in merchant's database to 'expire'
                // $donation->addUpdate("Payment using " . $type . " for transaction order_id: " . $orderId . " is expired.");
                $pay->setExpired();
                $payId->status = "CANCEL";
                $payId->save();
            } elseif ($transaction == 'cancel') {
                // TODO set payment status in merchant's database to 'Failed'
                // $donation->addUpdate("Payment using " . $type . " for transaction order_id: " . $orderId . " is canceled.");
                $pay->setCancel();
                $payId->status = "CANCEL";
                $payId->save();
            } elseif ($transaction == 'failure') {
                $pay->setFailure();
                $payId->status = "CANCEL";
                $payId->save();
            }
        });
        return;
    }

    public function payAction($id)
    {
        $user = Order::where('id', '=', $id)->value('user_id');
        $userData = \App\User::findOrfail($user);
        $payId = DB::table('orders')->where('id', '=', $id)->value('pay_id');
        if ($payId == null) {
            $newPay = new Pay();
            $newPay->name = $userData->name;
            $newPay->email = $userData->email;
            $newPay->status = "cancel";
            $newPay->save();
            $orderUpdate = Order::findOrFail($id);
            $orderUpdate->pay_id = $newPay->id;
            $orderUpdate->save();
        } else {
            $pay = Pay::findOrFail($payId);
            $payStatus = Pay::where('id', '=', $payId)->value('status');
            if ($payStatus == 'submit') {
                $pay->status = "cancel";
                $pay->save();
            } elseif ($payStatus == 'pending') {
                return Veritrans_Transaction::cancel($id);
            }
        }
    }
    // End Custom
}
