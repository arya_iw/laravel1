<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use App\Book;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('manage-books');
        $keyword = $request->get('keyword') ? $request->get('keyword') : '';
        $status = $request->get('status');
        if ($status) {
            $book = Book::with('categories')->where("title", "like", "%$keyword%")->where('status', strtoupper($status))->paginate(4);
        } else {
            $book = Book::with('categories')->where("title", "like", "%$keyword%")->paginate(4);
        }
        return view('books.index', ['book'=>$book]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('manage-books');
        return \view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('manage-books');
        \Validator::make($request->all(), [
            "title" => "required|min:5|max:200",
            "description" => "required|min:20|max:1000",
            "author" => "required|min:3|max:100",
            "publisher" => "required|min:3|max:200",
            "price" => "required|digits_between:0,10",
            "stock" => "required|digits_between:0,10",
            "cover" => "required"
        ])->validate();
        $new_book = new Book();
        $new_book->title = $request->get('title');
        $new_book->description = $request->get('description');
        $new_book->author = $request->get('author');
        $new_book->publisher = $request->get('publisher');
        $new_book->price = $request->get('price');
        $new_book->stock = $request->get('stock');
        $new_book->status = $request->get('save_action');
        $cover = $request->file('cover');
        if ($cover) {
            $cover_path = $cover->store('book-covers', 'public');
            $new_book->cover = $cover_path;
        }
        $new_book->slug = \Str::slug($request->get('title'));
        $new_book->created_by = \Auth::user()->id;
        $new_book->save();
        $new_book->categories()->attach($request->get('categories'));
        if ($request->get('save_action') == 'PUBLISH') {
            return \redirect()->route('books.create')->with('status', 'Book successfully save and published');
        } elseif ($request->get('save_action') == 'DRAFT') {
            return \redirect()->route('books.create')->with('status', 'Book saved as draft');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('manage-books');
        $book = Book::findOrFail($id);
        return \view('books.edit', ['book'=>$book]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('manage-books');
        $book = Book::findOrFail($id);
        \Validator::make($request->all(), [
            "title" => "required|min:5|max:200",
            "slug" => [
                "required",
                Rule::unique("books")->ignore($book->slug, "slug")
            ],
            "description" => "required|min:20|max:1000",
            "author" => "required|min:3|max:100",
            "publisher" => "required|min:3|max:200",
            "price" => "required|digits_between:0,10",
            "stock" => "required|digits_between:0,10"
        ])->validate();
        $book->title = $request->get('title');
        $book->slug = $request->get('slug');
        $book->description = $request->get('description');
        $book->author = $request->get('author');
        $book->publisher = $request->get('publisher');
        $book->stock = $request->get('stock');
        $book->price = $request->get('price');
        $new_cover = $request->file('cover');
        if ($new_cover) {
            if ($book->cover && file_exists(storage_path('app/public/' .$book->cover))) {
                \Storage::delete('public/'. $book->cover);
            }
            $new_cover_path = $new_cover->store('book-covers', 'public');
            $book->cover = $new_cover_path;
        }
        $book->updated_by = \Auth::user()->id;
        $book->status = $request->get('status');
        $book->save();
        $book->categories()->sync($request->get('categories'));
        return redirect()->route('books.edit', [$book->id])->with('status', 'Book successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('manage-books');
        $book = Book::findOrFail($id);
        $book->deleted_by = \Auth::user()->id;
        $book->save();
        $book->delete();
        return redirect()->route('books.index')->with('status', 'Book moved to trash')->with('type', 'warning');
    }

    public function trash()
    {
        $this->authorize('manage-books');
        $book_trash = Book::onlyTrashed()->paginate(10);
        return view('books.trash', ['book_trash'=>$book_trash]);
    }

    public function restore($id)
    {
        $this->authorize('manage-books');
        $book = Book::withTrashed()->findOrFail($id);
        if ($book->trashed()) {
            $book->deleted_by = null;
            $book->save();
            $book->restore();
        } elseif (!$book->trashed()) {
            return redirect()->route('books.trash')->with('status', 'Book is not in trash');
        }
        return redirect()->route('books.trash')->with('status', 'Book successfully restored')->with('type', 'success');
    }
    public function deletepermanent($id)
    {
        $this->authorize('manage-books');
        $check_book = DB::table('book_order')->where('book_id','=',$id)->count();
        $book = Book::withTrashed()->findOrFail($id);
        if (!$book->trashed()) {
            return redirect()->route('books.index')->with('status', 'can not delete permanent active book')->with('type','warning');
        } elseif($check_book > 0){
            return redirect()->route('books.trash')->with('status', 'cannot delete a book related to the order data')->with('type','warning');
        } elseif ($book->trashed()) {
            if ($book->cover && file_exists(storage_path('app/public/' . $book->cover))) {
                \Storage::delete('public/'.$book->cover);
            }
            $book->categories()->detach();
            $book->forceDelete();
        }
        return \redirect()->route('books.trash')->with('status', 'Category permanently deleted')->with('type', 'danger');
    }
    public function ajaxbooksearch(Request $request){
        $this->authorize('create-orders');
        $keyword = $request->get('q');
        $booksearch = Book::where('title','like',"%$keyword%")->get();
        return $booksearch;
    }
}
