<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Carbon\Carbon;

class exportPdf extends Controller
{
    public function show($id)
    {
        $this->authorize('create-orders');
        $transaction = \App\Order::with('pays')->with('user')->with('books')->with('ship')->where('id','=',$id)->get();
        return view('cetakPdf.printPdf',compact('transaction'));
    }

    public function cetak($id){
        $this->authorize('create-orders');
        $transaction = \App\Order::with('pays')->with('user')->with('books')->with('ship')->where('id','=',$id)->get();
        $pdf = PDF::loadview('cetakPdf.pdf',['transaction'=>$transaction]);
        return $pdf->download('Transaction_Detail.pdf');
    }

    public function showReportOrderPdf(Request $request){
        $this->authorize('manage-orders');
        if($request->export_action == "export_specific"){
            \Validator::make($request->all(),[
                "initial_date"=>"required",
                "end_date"=>"required"
            ])->validate();
            $end_date = Carbon::parse($request->end_date)->addDay(1)->format('Y-m-d');
            $export = \App\Order::with('books')->with('user')->with('ship')
            ->where('updated_at','>=',$request->initial_date)
            ->Where('updated_at','<=',$end_date)
            ->where('status','=','FINISH')
            ->get();
        }elseif($request->export_action == "export_all"){
            $export = \App\Order::with('books')->with('user')->with('ship')
            ->where('status','=','FINISH')->get();
        }
        // return $export;
        $totalAllPrice = $export->sum('total_price');
        \Session::put('initial_date',$request->initial_date);
        \Session::put('end_date',$request->end_date);
        return view('cetakPdf.reportOrders',compact('export','totalAllPrice'));
    }
    public function printReportOrderPdf(Request $request){
        $this->authorize('manage-orders');
        if(($request->initial_date == null) && ($request->end_date == null)){
            $export = \App\Order::with('books')->with('user')->with('ship')
            ->where('status','=','FINISH')->get();
        } else {
            $end_date = Carbon::parse($request->end_date)->addDay(1)->format('Y-m-d');
            $export = \App\Order::with('books')->with('user')->with('ship')
            ->where('updated_at','>=',$request->initial_date)
            ->Where('updated_at','<=',$end_date)
            ->where('status','=','FINISH')
            ->get();
        }
        $pdf = PDF::loadview('cetakPdf.printReportOrders',['export' => $export])->setPaper('a4', 'landscape');;
        return $pdf->download('report_order.pdf');
    }
}
