<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Kavist\RajaOngkir\Facades\RajaOngkir;
use Illuminate\Support\Facades\DB;
use auth;
use App\Order;
use App\Book;
use App\Province;
use App\City;
use App\Courier;
use App\Ship;
use App\User;

class OrderController extends Controller
{
    public function __construct(PayController $payAction)
    {
        $this->payAction = $payAction;
        // $this->middleware(function ($request, $next) {
        //     if (Gate::allows('manage-orders')) {
        //         return $next($request);
        //     }
        //     abort(403, 'Anda tidak memiliki cukup hak akses');
        // });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('manage-orders');
        $status = $request->get('status');
        $buyer_email = $request->get('buyer_email');
        // $orders = Order::with('user')->with('books')->whereHas('user', function ($query) use ($buyer_email) {
        //     $query->where('email', 'LIKE', "%$buyer_email%");
        // })->where('status', 'LIKE', "%$status%")->paginate(10);
        $orders = Order::with('user')
        ->with('pays')
        ->with('books')
        ->whereHas('user', function ($query) use ($buyer_email) {
            $query->where('email', 'LIKE', "%$buyer_email%");
        })
        ->where('status', 'LIKE', "%$status%")->orderBy('id', 'DESC')->paginate(10);
        return view('orders.index', ['orders'=>$orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create-orders');
        $data = Book::orderBy('id', 'desc')->paginate(6);
        return view('orders.home', compact('data'));
    }

    public function fetch_book_data(Request $request)
    {
        $this->authorize('create-orders');
        if ($request->ajax()) {
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = $request->get('query');
            $query = str_replace(" ", "%", $query);
            $data = Book::where('title', 'like', "%$query%")
                ->orWhere('slug', 'like', "%$query%")
                ->orWhere('description', 'like', "%$query%")
                ->orWhere('author', 'like', "%$query%")
                ->orWhere('publisher', 'like', "%$query%")
                ->orWhere('price', 'like', "%$query%")
                ->orderBy($sort_by, $sort_type)
                ->paginate(6);
            return \view('orders.card_book', compact('data'))->render();
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create-orders');
        \Validator::make($request->all(), [
            "province_destination" => "required_without:city_destination|not_in:0",
            "city_destination" => "required_without:province_destination|not_in:0",
            "couriers_destination" => "required_without:city_destination|not_in:0",
            "service" => "required_without:couriers_destination|not_in:0",
            "value" => "numeric|min:1"
        ], [
            "value.min" => "value cannot be empty, please select the service field."
        ])->validate();
        //table ship
        $origin = User::where('email', 'like', 'administrator@larashop.test')->value('address');
        $province = Province::where('province_id', 'like', $request->get('province_destination'))->value('title');
        $city = City::where('city_id', 'like', $request->get('city_destination'))->value('title');
        $ship = new Ship();
        $ship->origin_address = $origin;
        $ship->destination_address = $city.",".$province;
        $ship->courier = $request->get('couriers_destination');
        $ship->cost = $request->get('value');
        $ship->save();
        // table order
        $book = Book::findOrFail($request->get('book_id'));
        $invoice_number = IdGenerator::generate(['table' => 'orders', 'field' => 'invoice_number','length' => 10, 'prefix' =>'ODR']);
        $orders = new Order();
        $orders->user_id = \Auth::user()->id;
        $orders->total_price = $book->price * $request->get('quantity') + $request->get('value');
        $orders->invoice_number = $invoice_number;
        $orders->ship()->associate($ship);
        $orders->save();
        $orders->books()->attach($request->book_id, ['quantity'=>$request->quantity]);
        $data = Book::orderBy('id', 'desc')->paginate(6);
        return redirect()->route('orders.personal', [$request->book_id])->with('status', 'Order status Successfully Created')->with('type', 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $couriers = Courier::pluck('title', 'code');
        $provinces = Province::pluck('title', 'province_id');
        $book = Book::with('categories')->findOrFail($id);
        return view('orders.show', compact('book', 'couriers', 'provinces'));
    }

    public function getCities($id)
    {
        $city = City::where('province_id', 'like', $id)->pluck('title', 'city_id');
        return json_encode($city);
    }

    public function ongkir(Request $request)
    {
        $cost = RajaOngkir::ongkosKirim([
            'origin' => 487,
            'destination' => $request->city_destination,
            'weight' => 1000,
            'courier' => $request->courier
        ])->get();
        return $cost;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('manage-orders');
        $orders = Order::findOrFail($id);
        $ship = Ship::findOrFail($orders->ship_id);
        return view('orders.edit', ['orders'=>$orders,'ship' => $ship]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('manage-orders');
        $order = Order::findOrFail($id);
        $pay = Order::with('pays')->where('id', '=', $id)->get();
        if($order->status == $request->get('status')){
            return \redirect()->route('orders.edit',[$order->id])->with('status','you dont change anything')->with('type','warning');
        } elseif ($request->status == "PROCESS") {
            if ($pay[0]['pays']['status'] == "success") {
                $order->status = $request->get('status');
                $order->save();
                return redirect()->route('orders.edit', [$order->id])->with('status', 'Order status Successfully Updated')->with('type', 'success');
            } elseif (($pay[0]['pay_id'] == null) || ($pay[0]['pays']['status'] == "pending") || ($pay[0]['pays']['status'] == "submit")) {
                return redirect()->route('orders.edit', [$order->id])->with('status', 'Updated order status failed make sure the order has been paid for.')->with('type', 'warning');
            } else {
                return redirect()->route('orders.edit', [$order->id])->with('status', 'Order status failed Updated payment of your order has '.$pay[0]['pays']['status'])->with('type', 'warning');
            }
        } elseif($request->status == "CANCEL"){
            if ($pay[0]['pays']['status'] == "success") {
                return redirect()->route('orders.edit', [$order->id])->with('status', 'Order status cannot be updated because our system cannot return orders that have already been paid for.')->with('type', 'warning');
            } elseif (($pay[0]['pays']['status'] == "failed") || ($pay[0]['pays']['status'] == "failure") || ($pay[0]['pays']['status'] == "cancel") || ($pay[0]['pays']['status'] == "expired")) {
                $order->status = $request->get('status');
                $order->save();
                return redirect()->route('orders.edit', [$order->id])->with('status', 'Order status Successfully Updated')->with('type', 'success');
            } elseif (($pay[0]['pays']['status'] == "pending") || ($pay[0]['pays']['status'] == "submit") || ($pay[0]['pay_id'] == null)){
                $this->payAction->payAction($id);
                $order->status = $request->get('status');
                $order->save();
                return redirect()->route('orders.edit', [$order->id])->with('status', 'Order status Successfully Updated')->with('type', 'success');
            }
        } elseif($request->status == "SUBMIT"){
            if(($order->status == "FINISH") || ($order->status == "CANCEL")){
                return redirect()->route('orders.edit', [$order->id])->with('status', 'Order status that has been canceled and that has been completed cannot be changed again')->with('type', 'warning');
            } else{
                $order->status = $request->get('status');
                $order->save();
                return redirect()->route('orders.edit', [$order->id])->with('status', 'Order status Successfully Updated')->with('type', 'success');
            }
        } elseif ($request->status == "FINISH") {
            if ($pay[0]['pays']['status'] == "success") {
                if($order->status == "PROCESS"){
                    $order->status = $request->get('status');
                    $order->save();
                } elseif($order->status == "SUBMIT"){
                    return \redirect()->route('orders.edit',[$order->id])->with('status','Orders cannot be updated, items must be processed before they are declared complete.')->with('type','warning');
                }
                return redirect()->route('orders.edit', [$order->id])->with('status', 'Order status Successfully Updated')->with('type', 'success');
            } elseif (($pay[0]['pay_id'] == null) || ($pay[0]['pays']['status'] == "pending") || ($pay[0]['pays']['status'] == "submit")) {
                return redirect()->route('orders.edit', [$order->id])->with('status', 'Updated order status failed make sure the order has been paid for.')->with('type', 'warning');
            } else {
                return redirect()->route('orders.edit', [$order->id])->with('status', 'Order status failed Updated payment of your order has '.$pay[0]['pays']['status'])->with('type', 'warning');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function personalOrder(Request $request)
    {
        $this->authorize('create-orders');
        $status = $request->get('status');
        $buyer_email = \Auth::user()->id;
        $orders = Order::with('user')
        ->with('pays')
        ->with('books')
        ->whereHas('user', function ($query) use ($buyer_email) {
            $query->where('id', '=', $buyer_email);
        })
        ->where('status', 'LIKE', "%$status%")->orderBy('id', 'DESC')->paginate(10);
        return view('personalOrder.index', ['orders' => $orders]);
    }
    public function personalOrderView($id)
    {
        $this->authorize('create-orders');
        $orders = Order::findOrFail($id);
        $ship = Ship::findOrFail($orders->ship_id);
        return view('personalOrder.show', compact('orders', 'ship'));
    }
    public function personalOrderUpdate($id)
    {
        $this->authorize('create-orders');
        $this->payAction->payAction($id);
        $orders = Order::findOrFail($id);
        $orders->status = "CANCEL";
        $orders->save();
        return redirect()->route('orders.personal')->with('status', 'Order Successfully Canceled')->with('type', 'success');
    }
}
// elseif(($pay[0]['pays']['status'] == "failed") || ($pay[0]['pays']['status'] == "failure") || ($pay[0]['pays']['status'] == "cancel") || ($pay[0]['pays']['status'] == "expired") ) {
            //     return redirect()->route('orders.edit', [$order->id])->with('status', 'Order status failed Updated payment of your order has '.$pay[0]['pays']['status'])->with('type', 'warning');
            // }
