<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\User;
use App\Message;
use Auth;
use Carbon\Carbon;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (Gate::allows('create-messages')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    public function contactOwner()
    {
        $contacts = User::where('id', 'not like', Auth::user()->id)
        ->where('roles', 'like', '["ADMIN"]')->orWhere('roles', 'like', '["STAFF"]')->get();
        return $contacts;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fileter_person = $request->filter;
        $username = Auth::user()->username;
        if ($fileter_person) {
            if ($request->roles) {
                $chat = User::where('id', 'not like', Auth::user()->id)
                ->where('roles', 'like', json_encode($request->roles))
                ->where('name', 'like', "%$fileter_person%")
                ->paginate(6);
            } else {
                $chat = User::where('id', 'not like', Auth::user()->id)
                ->where('name', 'like', "%$fileter_person%")
                ->orWhere('email', 'like', "%$fileter_person%")
                ->where('name', '!=', Auth::user()->name)
                ->where('email', '!=', Auth::user()->email)
                ->paginate(6);
            }
        } elseif ($request->roles) {
            $chat = User::where('roles', 'like', json_encode($request->roles))
            ->where('id','not like',Auth::user()->id)
            ->paginate(6);
        } else {
            $chat_recant1 = Message::where('receive', '=', Auth::user()->id)->get('send')->toArray();
            $chat_recant2 = Message::where('send', '=', Auth::user()->id)->get('receive')->toArray();
            $chat_recant = array_merge($chat_recant1, $chat_recant2);
            if ($chat_recant) {
                $chat = User::where('id', 'not like', Auth::user()->id)
                ->whereIn('id', $chat_recant)
                ->orderBy('created_at', 'desc')
                ->paginate(6);
            } else {
                $chat = User::where('id', 'not like', Auth::user()->id)->paginate(6);
            }
        }
        return view('messages.index', ['chat' => $chat]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(), [
            "message" => "required_without:img_message",
            "img_message" => "required_without:message"
        ])->validate();
        $chat = new Message();
        $chat->send = \Auth::user()->id;
        $chat->receive = $request->sender_id;
        $chat->message = $request->message;
        if ($request->file('img_message')) {
            $file = $request->file('img_message')->store('img_messages', 'public');
            $chat->img_message = $file;
        }
        $chat->save();
        $timestamp_created_at_receive = User::findOrFail($request->sender_id);
        $timestamp_created_at_receive->created_at = Carbon::now()->toDateTimeString();
        $timestamp_created_at_receive->save();
        $timestamp_created_at_send = User::findOrFail(Auth::user()->id);
        $timestamp_created_at_send->created_at = Carbon::now()->toDateTimeString();
        $timestamp_created_at_send->save();
        return \redirect()->route('messages.show', [$request->sender_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_id = \Auth::user()->id;
        $messages = Message::where('send', '=', \Auth::user()->id)
                ->where('receive', '=', $id)
                ->orWhere('send', '=', $id)
                ->where('receive', '=', \Auth::user()->id)
                ->get();
        $chat_atribut = User::where('id', 'like', $id)->get();
        return view('messages.view', ['messages'=>$messages,'chat_atribut'=>$chat_atribut,'id'=>$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $chat =Message::findOrFail($id);
        if ($chat->img_message && file_exists(storage_path('app/public/' . $chat->img_message))) {
            \Storage::delete('public/'.$chat->img_message);
        }
        $chat->delete();
        return \redirect()->route('messages.show', [$chat->receive]);
    }
}
