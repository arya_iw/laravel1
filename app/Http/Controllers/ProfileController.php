<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = User::findOrFail($id);
        return $profile;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile = User::findOrFail($id);
        $profile->name = $request->name;
        $profile->phone = $request->phone;
        $profile->address = $request->address;
        $gambar = $request->file("avatar");
        if ($request->file("avatar")) {
            if ($profile->avatar && file_exists(storage_path('app/public/' . $profile->avatar))) {
                \Storage::delete('public/'. $profile->avatar);
            }
            $file = $request->file('avatar')->store('avatars', 'public');
            $profile->avatar = $file;
        }
        if ($request->password) {
            if ($request->current_password) {
                if (Hash::check($request->current_password, $profile->password)) {
                    $password = Hash::make($request->password);
                    $profile->password = $password;
                } else {
                    return $request->phone;
                }
            } else {
                return $request->name;
            }
        }
        $profile->save();
        return response()->json([
            "status" => "success",
            "message" => "Profile data successfully updated"
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
