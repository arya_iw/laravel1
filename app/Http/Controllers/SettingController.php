<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function sliderHome(Request $request)
    {
        $button_slider = 0;
        $slider = \App\Slider::all();
        $categorie = \App\Category::orderBy('id', 'DESC')->get();
        $book = \App\Book::where('stock', '>', 0)->paginate(12);
        if ($request->keyword && $request->price) {
            $query = $request->keyword;
            $query = str_replace(" ", "%", $query);
            $book = \App\Book::where('title', 'like', "%$query%")
                ->where('stock', '>', 0)
                ->orderBy('price', $request->price)->paginate(24);
            return view('searchIndex', compact('book'));
        } elseif ($request->keyword) {
            $query = $request->keyword;
            $query = str_replace(" ", "%", $query);
            $book = \App\Book::where('title', 'like', "%$query%")
                ->where('stock', '>', 0)
                ->paginate(24);
            return view('searchIndex', compact('book'));
        }
        return view('index', compact('slider', 'button_slider', 'categorie', 'book'));
    }

    public function sliderIndex()
    {
        $this->authorize('manage-orders');
        $slider = \App\Slider::paginate(5);
        return view('sliders.index', compact('slider'));
    }

    public function sliderCreate()
    {
        $this->authorize('manage-orders');
        return view('sliders.create');
    }

    public function sliderStore(Request $request)
    {
        $this->authorize('manage-orders');
        \Validator::make($request->all(), [
            "name_slider" => "required",
            "image" => "required"
        ])->validate();
        $new_slider = new \App\Slider;
        $new_slider->name_slider = $request->name_slider;
        if ($request->file('image')) {
            $file = $request->file('image')->store('img_slider', 'public');
            $new_slider->img_slider = $file;
        }
        $new_slider->save();
        return redirect()->route('slider.index')->with('status', 'Slider successfully created')->with('type', 'success');
    }

    public function sliderDestroy($id)
    {
        $this->authorize('manage-orders');
        $slider = \App\Slider::findOrFail($id);
        if ($slider->img_slider && file_exists(storage_path('app/public/'.$slider->img_slider))) {
            \Storage::delete('public/'.$slider->img_slider);
        }
        $slider->delete();
        return \redirect()->route('slider.index');
    }

    public function cardCategorie($slug)
    {
        $categorie = \App\Category::where('slug', '=', $slug)->get();
        $book = \App\Book::with('categories')
        ->whereHas('categories', function ($query) use ($slug) {
            $query->where('slug', '=', $slug);
        })
        ->where('stock', '>', 0)
        ->paginate(12);
        return \view('categories.card', compact('book', 'categorie'));
    }
}
