<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use softDeletes;
    protected $table = "books";
    protected $primaryKey = "id";
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function orders(){
        return $this->belongsToMany('App\Order');
    }
}
