<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pay extends Model
{
    protected $table = "pays";
    protected $primaryKey = "id";

    public function orders(){
        return $this->hashOne('App\Order');
    }

    public function setPending()
    {
        $this->attributes['status'] = 'pending';
        self::save();
    }

    public function setSuccess()
    {
        $this->attributes['status'] = 'success';
        self::save();
    }

    public function setFailed()
    {
        $this->attributes['status'] = 'failed';
        self::save();
    }

    public function setFailure()
    {
        $this->attributes['status'] = 'failure';
        self::save();
    }

    public function setExpired()
    {
        $this->attributes['status'] = 'expired';
        self::save();
    }

    public function setCancel()
    {
        $this->attributes['status'] = 'cancel';
        self::save();
    }
}
