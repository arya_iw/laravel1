<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ship extends Model
{
    protected $table = "ships";
    protected $primaryKey = 'id';
    public function orders()
        {
            return $this->hashOne('App\Order');
        }
}

