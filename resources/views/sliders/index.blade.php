@extends('layouts.global2')
@section('title')
    All Slider
@endsection
@section('footer-scripts')

@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-{{session('type')}}">
            {{session('status')}}
        </div>
    </div>
</div>
<hr class="my-3">
<div class="row">
    <div class="col-md-12 overflow-x-y-75">
        <div class="row mb-3">
            <div class="col-md-12 text-right">
                <a href="{{route('slider.create')}}" class="btn btn-primary">Create Slider</a>
            </div>
        </div>
        <div class="table-responsive-md">
            <table class="table table-bordered table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Image Slider</th>
                        <th scope="col">Name Slider</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($slider as $sliders)
                        <tr>
                            <td><img src="/storage/{{ $sliders->img_slider }}" alt="" width="400px" height="150px"></td>
                            <td>{{ $sliders->name_slider }}</td>
                            <td>
                            <form onsubmit="return confirm('Delete this Img Slider permanently?')" class="d-inline"
                                action="{{route('slider.delete', [$sliders->id])}}" method="POST">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit"
                                    class="btn btn-danger btn-sm mt-lg-auto mt-md-auto mt-sm-1 mt-xl-auto"><i
                                        class="fa fa-trash fa-lg"></i></button>
                            </form>
                        </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=10>
                            {{$slider->appends(Request::all())->links()}}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
