@extends('layouts.global2')
@section('title')
Create Orders
@endsection
@section('footer-scripts')
<script src="{{asset('/js/orders_home.js')}}"></script>
@endsection
@section('content')
<div class="container-fluid m-0 p-0 bg-white shadow-lg">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-8">
            <div class="input-group pl-4 pt-4">
                <input type="text" class="form-control search">
                <div class="input-group-append" id="button-addon4">
                    <button data-sort="price" data-sort_type="desc" class="btn btn-outline-secondary sorting"
                        type="button"><i id="price_icon" class="fas fa-sort"></i> Price</button>
                </div>
            </div>
        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="alert alert-warning mt-5 ALERT m-3 mt-0" role="alert" style="display: none">
                <h4 class="alert-heading">Not Found!</h4>
                <hr>
                <p>The book you are looking for does not exist, please enter data related to the book,
                    such as the name of the book, the name of the publisher, the price of the book etc.</p>
            </div>
        </div>
    </div>
    <div class="row card-book pt-4 m-1 overflow-x-y-75">
        @include('orders.card_book')
    </div>
    <input type="hidden" name="hidden_page" id="hidden_page" value="1">
    <input type="hidden" name="hidden_data_sort" id="hidden_data_sort" value="id">
    <input type="hidden" name="hidden_data_sort_type" id="hidden_data_sort_type" value="desc">
</div>
@endsection
