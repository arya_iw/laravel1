@extends('layouts.global2')
@section('title')
Create Orders
@endsection
@section('footer-scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $('#book').select2({
    ajax:{
    url:'http://127.0.0.1:8000/ajax/books/search',
    processResults: function(data){
    return {
    results: data.map(function(item){
        return{
            id:item.id,text:item.title
        }
        })
    }
    }
    }
    });
    $(document).ready(function(){
        $('.form-group').first().on('change','#book',function(){
            var select = $(this).find('option:selected').val();
            if(select){
                $('#quantity').prop('readonly',false);
            } else {
                $('#quantity').prop('readonly',true);
            }
            $('#quantity').val(0);
            $('#price').val(0);
            $('.row .col-md-4').empty();
        $.ajax({
        url:'http://127.0.0.1:8000/ajax/books/search',
        type:'GET',
        dataType:"json"
        }).done(function(data){
            $.each(data,function(index,array){
                if(array.id == select){
                    var book_value = array.price
                    let book_info = `
                    <div class="col-md-4">
                        <ul class="list-group book-data">
                            <li class="list-group-item book_title"><b>Book Title : </b>${this.title}</li>
                            <li class="list-group-item book-image text-center"><img src="/storage/${this.cover}" alt="" width="100px" height="100px"></li>
                            <li class="list-group-item book-author"><b>Book Author : </b>${this.author}</li>
                            <li class="list-group-item book-description"><b>Price : </b>${this.price}</li>
                            <li class="list-group-item book-description"><b>Description : </b>${this.description}</li>
                        </ul>
                    </div>`
                    $('.row .col-md-8').after(book_info);
                    $('input[name="book_id"]').val(array.id)
                    $('.form-group').eq(1).on('keyup click','#quantity',function(){
                        let quantity = $(this).val();
                        var total_price = book_value * quantity;
                        $('.form-group #price').val(total_price);
                    })
                }
            })
        })
        });
        $('.select2').css({
            'width' : '100%'
        });
    });
</script>
@endsection
@section('content')
<div class="row">
    <div class="col-md-8">
        <form action="{{route('orders.store')}}" enctype="multipart/form-data" class="bg-white shadow-sm p-3"
            method="POST">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="book">Book</label>
                    <select class="form-control form-control-lg" id="book" name="book">
                    </select>
                </div>
                <input type="hidden" name="book_id">
                <div class="form-group col-md-6">
                    <label for="quantity">Quantity</label>
                    <input class="form-control" type="number" name="quantity" id="quantity" readonly min="0" value="0" >
                </div>
                <div class="form-group col-md-6">
                    <label for="price">Total Price</label>
                    <input class="form-control" type="number" name="price" id="price" readonly min="0" value="0">
                </div>
            </div>
            <button type="submit" class="btn btn-primary text-right btn-order">Order</button>
        </form>
    </div>
</div>
@endsection
