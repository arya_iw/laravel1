@extends('layouts.global2')
@section('title')
All Order
@endsection
@section('footer-scripts')
<script
    src="{{ !config('services.midtrans.isProduction') ? 'https://app.sandbox.midtrans.com/snap/snap.js' : 'https://app.midtrans.com/snap/snap.js' }}"
    data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
<script>
    function paySubmit(id){
        $.post("{{ route('pay.submit') }}",
        {
            _method: 'POST',
            _token: '{{ csrf_token() }}',
            order_id : id,
        },
        function (data, status) {
            if(data == "stock"){
                $('.notif-stock').append(`
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="alert alert-warning mt-0" role="alert">
            <h4 class="alert-heading">less book stock</h4>
            <hr>
            <p>please contact admin to add book stock</p>
        </div>
                `);
            } else {
            snap.pay(data.snap_token, {
                // Optional
                onSuccess: function (result) {
                    location.reload();
                },
                // Optional
                onPending: function (result) {
                    location.reload();
                },
                // Optional
                onError: function (result) {
                    location.reload();
                }
            });
            }
        });
        return false;
    }
    $('.row-export').on('click','.btn-export',function(e){
                e.preventDefault();
                var btnExportStatus =  $('.btn-export').attr('data-btn_export')
                if(btnExportStatus == "hide"){
                    $('.btn-export').attr('data-btn_export','show')
                    $('.row-export').append(`
                    <div class="col-md-12 form-export">
                        <form action="{{ route('report.orders') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="initial_date">Initial Date</label>
                                        <input class="form-control @error('initial_date') is-invalid @enderror" type="date" name="initial_date" id="initial_date">
                                        @error('initial_date')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="end_date">End date</label>
                                        <input class="form-control @error('end_date') is-invalid @enderror" type="date" name="end_date" id="end_date">
                                        @error('end_date')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mt-2">
                                        <button class="btn btn-primary mt-4" name="export_action" value="export_specific">Export specific report</button>
                                        <button class="btn btn-secondary mt-4" name="export_action" value="export_all">Export all report</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>`)
                    $('.form-export').hide().slideDown(500);
                }else if(btnExportStatus == "show"){
                    $('.btn-export').attr('data-btn_export','hide')
                    $('.form-export').slideUp(500,function(){
                        this.remove();
                    })
                }
            })
</script>
@endsection
@section('content')
<form action="{{route('orders.index')}}">
    <div class="row notif-stock">

    </div>
    <div class="row row-export">
        <div class="col-md-5">
            <div class="form-group">
                <input class="form-control" value="{{Request::get('buyer_email')}}" type="text" name="buyer_email"
                    placeholder="Search by buyer email">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <select name="status" class="form-control" id="status">
                    <option value="">ANY</option>
                    <option {{Request::get('status') == "SUBMIT" ? "selected" : ""}} value="SUBMIT">SUBMIT</option>
                    <option {{Request::get('status') == "PROCESS" ? "selected" : ""}} value="PROCESS">PROCESS</option>
                    <option {{Request::get('status') == "FINISH" ? "selected" : ""}} value="FINISH">FINISH</option>
                    <option {{Request::get('status') == "CANCEL" ? "selected" : ""}} value="CANCEL">CANCEL</option>
                </select>
            </div>
        </div>
        <div class="col-md-1">
            <input class="btn btn-primary" type="submit" value="Filter">
        </div>
        <div class="col-md-4">
            <input type="button" class="btn btn-primary btn-export" data-btn_export="hide" value="Export Orders Report">
        </div>
    </div>
</form>
<div class="row">
    <div class="col-md-12 overflow-x-y-75">
        <hr class="my-3">
        <div class=" table-responsive-md">
            <table class="table table-bordered table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Invoice Number</th>
                        <th scope="col">Status</th>
                        <th scope="col">Buyer</th>
                        <th scope="col">Total quantity</th>
                        <th scope="col">Order date</th>
                        <th scope="col">Total Price</th>
                        <th scope="col">Status Pembayaran</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                    <tr>
                        <td>{{$order->invoice_number}}</td>
                        <td>
                            @if ($order->status == "SUBMIT")
                            <span class="badge badge-warning text-light">{{$order->status}}</span>
                            @elseif($order->status == "PROCESS")
                            <span class="badge badge-info text-light">{{$order->status}}</span>
                            @elseif($order->status == "FINISH")
                            <span class="badge badge-success text-light">{{$order->status}}</span>
                            @elseif($order->status == "CANCEL")
                            <span class="badge badge-dark text-light">{{$order->status}}</span>
                            @endif
                        </td>
                        <td>
                            {{$order->user->name}} <br>
                            <small>{{$order->user->email}}</small>
                        </td>
                        <td>{{$order->totalQuantity}} pc (s)</td>
                        <td>{{$order->created_at}}</td>
                        <td>{{$order->total_price}}</td>
                        <td>
                            @if ($order->pays == "")
                            <button class="btn btn-sm btn-success" onclick="paySubmit({{ $order->id }})">Pay</button>
                            @else
                            @if($order->pays->status == "success")
                            <span class="badge badge-success text-light">{{$order->pays->status}}</span>
                            <br>
                            <a class="small text-primary" href="{{ route('transaction.details',[$order->id]) }}"><u>Export PDF</u></a>
                            @elseif(($order->pays->status == "pending") || ($order->pays->status == "submit"))
                            <button class="badge badge-warning text-light border-0"
                                onclick="snap.pay('{{ $order->pays->snap_token }}')">Complete Payment</button>
                            @elseif(($order->pays->status == "expired") || ($order->pays->status == "failure") ||
                            ($order->pays->status == "cancel") || ($order->pays->status == "failed"))
                            <span class="badge badge-danger text-light">{{$order->pays->status}}</span>
                            @endif
                            @endif
                        </td>
                        <td>
                            <a class="btn btn-info btn-sm" href="{{route('orders.edit',[$order->id])}}"><span
                                    class="oi oi-pencil text-white"></span></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">
                            {{$orders->appends(Request::all())->links()}}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
