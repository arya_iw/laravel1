@extends('layouts.app')
@section('script')
<script src="{{asset('/js/orders_show.js')}}"></script>
<script src="{{asset('/js/messages_contact.js')}}"></script>
<script>
    $('.navbar-expand-md').addClass('fixed-top');
    $('body').css('background-color','rgba(128, 128, 128, 0.212)')
    var maxVal = $('#quantity').attr('max');
    $('#quantity').on('input', function () {
    var value = $(this).val();
    if ((value !== '') && (value.indexOf('.') === -1)) {
        $(this).val(Math.max(Math.min(value, maxVal), 1));
    }
})
</script>
@endsection
@section('home-content')
<div class="container bg-white shadow-lg mt-5">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-12 p-4 text-center">
            <img src="/storage/{{$book->cover}}" class="img-thumbnail img img-cover" alt="">
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-12 p-4">
            <h4>{{$book->title}}</h4>
            <p class="font-weight-bold" id="book-price" data-price="{{$book->price}}">IDR
                {{number_format($book->price)}}</p>
            <div class="form-row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-3">
                    <div class="form-group">
                        <label for="quantity">Quantity</label>
                        <input class="form-control" type="number" name="quantity" id="quantity" value="1" min="1"
                            max="{{ $book->stock }}">
                    </div>
                </div>
            </div>
            <form action="{{route('orders.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="book_id" value="{{$book->id}}">
                <input type="hidden" name="quantity">
                <div class="form-row">
                    <div class="col-md-4 col-sm-4 col-4">
                        <div class="form-group">
                            <label for="province_destination">Send To</label>
                            <select class="form-control @error('province_destination') is-invalid @enderror"
                                id="province_destination" name="province_destination">
                                <option value="0">Provinsi Tujuan</option>
                                @foreach ($provinces as $province => $value)
                                <option value="{{$province}}">{{$value}}</option>
                                @endforeach
                            </select>
                            @error('province_destination')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-4">
                        <div class="form-group">
                            <label for="city_destination">&MediumSpace;</label>
                            <select class="form-control @error('city_destination') is-invalid @enderror"
                                id="city_destination" name="city_destination">
                                <option value="0">Kota Tujuan</option>
                            </select>
                            @error('city_destination')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-4">
                        <div class="form-group">
                            <label for="couriers_destination">&MediumSpace;</label>
                            <select class="form-control @error('couriers_destination') is-invalid @enderror"
                                id="couriers_destination" name="couriers_destination">
                                <option value="0" selected="selected">Kurir</option>
                                @foreach ($couriers as $courier => $value)
                                <option value="{{$courier}}">{{$value}}</option>
                                @endforeach
                            </select>
                            @error('couriers_destination')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-6">
                        <div class="form-group">
                            <select class="form-control @error('service') is-invalid @enderror" id="service"
                                name="service">
                                <option value="0">Service</option>
                            </select>
                            @error('service')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-6">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Biaya Ongkir</span>
                            </div>
                            <input type="number" class="form-control @error('value') is-invalid @enderror"
                                aria-describedby="basic-addon1" name="value" min="0" readonly>
                            @error('value')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-12">
                        @if (Auth::check())
                        <input type="submit" value="buy" name="buy" class="btn btn-primary mt-n2 mb-2 pl-5 pr-5">
                        @else
                        <a href="{{route('login')}}" class="btn btn-primary mt-n2 mb-2 pl-5 pr-5">Buy</a>
                        @endif
                        @if (Auth::check())
                        <input type="button" value="Contact Owner" class="btn btn-primary mt-n2 mb-2"
                            onclick="contactOwner()">
                        @else
                        <a href="{{route('login')}}" class="btn btn-primary mt-n2 mb-2 pl-5 pr-5">Contact Owner</a>
                        @endif
                    </div>
                </div>
            </form>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                        aria-controls="home" aria-selected="true">Description Book</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                        aria-controls="profile" aria-selected="false">Product details</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    {{$book->description}}</div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <ul>
                        <li><span class="font-weight-bold">Book Title : </span>{{$book->title}}</li>
                        <li><span class="font-weight-bold">Author : </span>{{$book->author}}</li>
                        <li><span class="font-weight-bold">Publisher : </span>{{$book->publisher}}</li>
                        <li><span class="font-weight-bold">Categories : </span>
                            @foreach ($book->categories as $categories)
                            {{$categories->name}},
                            @endforeach
                        </li>
                        <li><span class="font-weight-bold">price : </span>{{$book->price}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@include('modal.messages_owner')
@endsection
