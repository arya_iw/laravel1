@foreach ($data as $item)
<div class="col-lg-2 col-md-3 col-sm-4 col-6 pb-4">
    <div class="card">
        <img src="/storage/{{$item->cover}}" alt="" class="img-thumbnail img-fluid">
        <div class="card-body">
            <p class="card-title text-truncate">{{$item->title}}</p>
            <p class="card-text">Rp.{{$item->price}}</p>
        <a href="{{route('orders.show',[$item->id])}}" class="btn btn-primary btn-sm  btn-block text-white">More</a>
        </div>
    </div>
</div>
@endforeach
<div class="col-lg-12 col-md-12 col-sm-12 col-12">
{{$data->appends(Request::all())->links()}}
</div>
