@extends('layouts.global2')
@section('title')
Edit Order
@endsection
@section('footer-scripts')
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        @if (session('status'))
        <div class="alert alert-{{session('type')}}">
            {{session('status')}}
        </div>
        @endif
    </div>
</div>
<div class="row shadow-lg bg-white m-1 rounded p-1">
    <div class="col-md-6">
        <form action="{{route('orders.update',[$orders->id])}} " method="post" enctype="multipart/form-data"
            class="">
            @csrf
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group">
                <label for="invoice_number">Invoice Number</label>
                <input class="form-control" value="{{$orders->invoice_number}}" type="text" name="invoice_number"
                    id="invoice_number" disabled>
            </div>
            <div class="form-group">
                <label for="buyer">Pembeli</label>
                <input class="form-control" value="{{$orders->user->name}}" type="text" name="buyer" id="buyer"
                    disabled>
            </div>
            <div class="form-group">
                <label for="created_at">Order Date</label>
                <input class="form-control" value="{{$orders->created_at}}" type="text" name="created_at"
                    id="created_at" disabled>
            </div>
            <label for="">Books ({{$orders->totalQuantity}}) </label><br>
            <ul>
                @foreach($orders->books as $book)
                <li>{{$book->title}} <b>({{$book->pivot->quantity}})</b></li>
                @endforeach
            </ul>
            <div class="form-group">
                <label for="total_price">Total Price</label>
                <input class="form-control" value="{{$orders->total_price}} " type="text" name="total_price"
                    id="total_price" disabled>
            </div>
            <div class=" form-group">
                <label for="status">Status</label><br>
                <select class="form-control" name="status" id="status">
                    <option {{$orders->status == "SUBMIT" ? "selected" : ""}} value="SUBMIT">SUBMIT</option>
                    <option {{$orders->status == "PROCESS" ? "selected" : ""}} value="PROCESS">PROCESS</option>
                    <option {{$orders->status == "FINISH" ? "selected" : ""}} value="FINISH">FINISH</option>
                    <option {{$orders->status == "CANCEL" ? "selected" : ""}} value="CANCEL">CANCEL</option>
                </select>
            </div>
            <input class="btn btn-primary" type="submit" value="Update">
        </form>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="origin_address">Origin Address</label>
            <textarea id="origin_address" class="form-control" rows="2" name="origin_address"
                disabled>{{$ship->origin_address}}</textarea>
        </div>
        <div class="form-group">
            <label for="destination_address">Destination Addres</label>
            <textarea id="destination_address" class="form-control" rows="2" name="destination_address"
                disabled>{{$ship->destination_address}}</textarea>
        </div>
        <div class="form-group">
            <label for="courier">Courier</label>
            <input class="form-control" type="text" name="courier" id="courier" value="{{$ship->courier}}" disabled>
        </div>
        <div class="form-group">
            <label for="cost">Cost</label>
            <input class="form-control" type="text" name="cost" id="cost" value="{{$ship->cost}}" disabled>
        </div>
    </div>
</div>
@endsection
