<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>BookStore @yield("title")</title>
    {{-- Fontawesome Icon --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css"
        integrity="sha384-xxzQGERXS00kBmZW/6qxqJPyxW3UR0BPsL4c8ILaIWXva5kFi7TxkIIaMiKtqV1Q" crossorigin="anonymous">
    {{-- Fontawesome Icon --}}
    {{-- Bootstrap CDN --}}
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    {{-- Bootstrap CDN --}}
    {{-- Polished Icon --}}
    <link rel="stylesheet" href="{{asset('polished/iconic/css/open-iconic-bootstrap.min.css')}}">
    {{-- Polished Icon --}}
    {{-- Sidebar --}}
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    {{-- Sidebar --}}
</head>

<body>
    <div class="wrapper d-flex align-items-stretch">
        @if (Auth::user())
        <nav id="sidebar">
            <div class="p-4">
                @if (\Auth::user())
                <a href="#" class="img logo rounded-circle mb-5"
                    style="background-image: url(/storage/{{Auth::user()->avatar}});background-size: cover;background-position: center center;"></a>
                @endif
                <ul class="list-unstyled components mb-5">
                    <li class="">
                        <a href="{{ route('home') }}"><span class="oi oi-home"></span> Home</a>
                    </li>
                    @can('create-messages')
                    <li><a href="{{route('messages.index')}}"><span class="fas fa-comment-alt"></span> Messages</a></li>
                    @endcan
                    @can('create-orders')
                    <li><a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false"
                            class="dropdown-toggle"><span class="oi oi-inbox"></span> Order</a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            @can('create-orders')
                            <li><a href="{{route('index')}}"><span class="fa fa-plus"></span> Create Order</a>
                            </li>
                            <li><a href="{{route('orders.personal')}}"><span class="fas fa-dolly-flatbed"></span> My Order</a>
                            </li>
                            @endcan
                            @can('manage-orders')
                            <li><a href="{{ route('orders.index') }} "><span class="oi oi-inbox"></span> Manage
                                    Order</a></li>
                            @endcan
                        </ul>
                    </li>
                    @endcan
                    @can('manage-users')
                    <li><a href="{{ route('users.index') }} "><span class="oi oi-people"></span> Manage User</a></li>
                    @endcan
                    @can('manage-categories')
                    <li><a href="{{ route('categories.index')}} "><span class="oi oi-tag"></span> Manage Category</a>
                    </li>
                    @endcan
                    @can('manage-books')
                    <li><a href="{{ route('books.index') }} "><span class="oi oi-book"></span> Manage Book</a></li>
                    @endcan
                    @can('manage-orders')
                    <li><a href="#setting" data-toggle="collapse" aria-expanded="false"
                        class="dropdown-toggle"><span class="fas fa-cog"></span> Setting</a>
                    <ul class="collapse list-unstyled" id="setting">
                        <li><a href="{{ route('slider.index') }}"><span class="fas fa-sliders-h"></span> Slider</a></li>
                    </ul>
                </li>
                    @endcan
                </ul>
                <div class="footer"></div>
            </div>
        </nav>
        @endif
        <!-- Page Content  -->
        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div class="container-fluid">
                    @if (\Auth::user())
                    <button type="button" id="sidebarCollapse" class="btn btn-primary">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                    @endif
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fa fa-bars"></i>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            @if (Auth::user())
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdownMenuLink"
                                    role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="/storage/{{Auth::user()->avatar}}" class="rounded-circle img-icon-profile"
                                        alt="">
                                    {{Auth::user()->name}}&MediumSpace;&MediumSpace;
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="#" onclick="editProfile({{Auth::user()->id}})"><i
                                            class="far fa-user"></i> Profile</a>
                                    <form action="{{route("logout")}}" method="POST" class="form-inline">
                                        @csrf
                                        <button class="dropdown-item" style="cursor:pointer"><i
                                                class="fas fa-sign-out-alt"></i> Sign Out</button>
                                    </form>
                                </div>
                            </li>
                            @else
                            <li class="nav-item">
                            <a class="nav-link active" href="{{route('login')}}"><i class="fas fa-sign-in-alt"></i> Login</a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link" href="{{route('register')}}"><i class="fas fa-sign-in-alt"></i> Register</a>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container-fluid mb-5">
                @yield("content")
            </div>
            @auth
            @include('modal.profile_modal')
            @endauth
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
        integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
        integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/additional-methods.js"></script>
    <script src="{{asset('sidebar/js/main.js')}}"></script>
    <script src="{{asset('js/profile.js')}}"></script>
    @yield('footer-scripts')
</body>

</html>
