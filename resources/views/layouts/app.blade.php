<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9 no-js" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BookStore @yield("title")</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css"
        integrity="sha384-xxzQGERXS00kBmZW/6qxqJPyxW3UR0BPsL4c8ILaIWXva5kFi7TxkIIaMiKtqV1Q" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('polished/iconic/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <style>
        .navbar {
            padding: 0.5rem 1rem;
        }

        .rounded-login {
            border-radius: 3%;
        }

        .form-control,
        .input-group-text {
            border: none;
            box-shadow: none;
            border-bottom: 1px solid #ced4da;
            border-radius: 0px;
        }

        .form-control:focus,
        .is-invalid:focus {
            box-shadow: none !important;
            border-color: orange !important;
        }

        .border-warning,
        .is-invalid {
            border-color: orange !important;
        }

        .card-header-circle {
            background-color: #f8b739;
            height: 100px;
            width: 100px
        }

        .card-icon {
            margin-top: 20%;
            width: 100%;
            height: 100%;
            font-size: 60px;
            color: white;
            align-items: center;
            text-align: center;
        }

        .border-bottom {
            border-bottom: 5px solid #dee2e6 !important;
        }

        .border-warning {
            border-color: orange !important;
        }

        .color-orange:hover {
            background-color: orange;
        }

        .color-orange {
            color: orange;
            border: 1px solid orange;
        }

        .dropdown-item:hover {
            background-color: orange;
        }

        .img-carousel {
            height: 400px !important;
        }

        .orange {
            background-color: orange !important;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark justify-content-between border-bottom border-warning">
        <a class="navbar-brand ml-lg-5" href="{{ route('index') }}">BookStore</a>
        {{-- <input class=" form-control d-none d-md-block w-50 ml-3 mr-2" type="text" placeholder="Search" aria-label="Search"> --}}
        <button class="btn btn-link d-block d-lg-none d-md-none" type="button" data-toggle="collapse"
            data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        {{-- <div class="ml-5 collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item d-xl-block d-lg-block d-md-none d-sm-none ml-5" style="width:100% !important; ">
                    <div class="input-group mt-1 ml-5">
                        <input type="text" class="form-control ml-5" placeholder="Search Book"
                            aria-describedby="basic-addon2" style="widows: 100px;dth: ">
                        <div class="input-group-append">
                            <button type="submit" class="btn color-orange btn-outline-secondary mr-lg-5"
                                type="button">Search</button>
                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown d-xl-block d-lg-block d-md-none d-sm-none">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Categories &MediumSpace;
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
            </ul>
        </div> --}}
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item ">
                    <form action="{{ route('index') }}" method="get">
                        <div class="input-group mt-1">
                            <input type="text" class="form-control ml-3" placeholder="Search Book"
                                aria-describedby="basic-addon2" name="keyword" value="{{Request::get('keyword')}}">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-outline-secondary color-orange"
                                    type="button">Search</button>
                            </div>
                        </div>
                    </form>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Categories &MediumSpace;
                    </a>
                    <div class="dropdown-menu bg-dark border border-warning" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item text-white" href="http://127.0.0.1:8000/card/php/categorie">PHP</a>
                        <a class="dropdown-item text-white" href="http://127.0.0.1:8000/card/mysql/categorie">MYSQL</a>
                        <a class="dropdown-item text-white" href="http://127.0.0.1:8000/card/javascript/categorie">Javascript</a>
                        <a class="dropdown-item text-white" href="http://127.0.0.1:8000/card/jquery/categorie">Jquery</a>
                        <a class="dropdown-item text-white" href="http://127.0.0.1:8000/card/vue-js/categorie">VUE JS</a>
                        <a class="dropdown-item text-white" href="http://127.0.0.1:8000/card/react-js/categorie">React js</a>
                        <a class="dropdown-item text-white" href="{{ route('index') }}">Another Category</a>
                    </div>
                </li>
                @if (Auth::user())
                <li class="nav-item mr-lg-1 mr-md-1">
                    <a class="nav-link" href="{{route('home')}}"><i class="fas fa-home"></i>
                        Dashboard</a>
                </li>
                <li class="nav-item">
                    <form action="{{route("logout")}}" method="POST" class="form-inline">
                        @csrf
                        <button class="pl-0 btn btn-lg btn-dark text-white-50"><i class="fas fa-sign-out-alt"></i> Sign
                            Out</button>
                    </form>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link active text-white" href="{{route('login')}}"><i class="fas fa-sign-in-alt"></i>
                        Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white-50" href="{{route('register')}}"><i class="fas fa-sign-in-alt"></i>
                        Register</a>
                </li>
                @endif
            </ul>
        </div>
    </nav>
    <section style="min-height: 100vh">
        <div class="container-fluid h-100 p-0">
            <div style="min-height: 100%" class="">
                <div class="col-lg-12 col-md-12 p-4">
                    @yield("content")
                </div>
            </div>
        </div>
        <div class="container-fluid">
            @yield('home-content')
        </div>
    </section>
    <!-- Footer -->
    <footer class="font-small bg-dark text-white mt-3">
        <div class="text-center py-3 orange">© 2020 Copyright: Arya Irama Wahono
        </div>
        <!-- Copyright -->
    </footer>
    <!-- Footer -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
        integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
        integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous">
    </script>
    <script>
        $(':input').on('focusin',function(){
            let id = this.id;
            if(id.length == 0){
                true;
            } else if (id.length > 0){
                $(`#${id}`).addClass('border-warning')
            }
        });
        $(':input').on('focusout',function(){
            let id = this.id;
            if(id.length == 0){
                true;
            } else if (id.length > 0){
                $(`#${id}`).removeClass('border-warning')
            }
        });
    </script>
    @yield("script")
</body>

</html>
