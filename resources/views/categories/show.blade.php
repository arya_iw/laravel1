@extends('layouts.global2')
@section('title')
Detail Category
@endsection
@section('content')
<div class="col-md-8 bg-white shadow rounded">
    <div class="row">
        <div class="col-md-3 text-center bg-primary-darker p-4">
            <section style="min-height:150px;">
                @if ($categories->image)
                <img class="img-fluid rounded-circle" src="{{asset('storage/'.$categories->image)}}" width="150px">
                @endif
            </section>
            <div class="mt-3">
                <a href="{{route('categories.edit',[$categories->id])}}" class="btn btn-info btn-sm"><span
                        class="oi oi-pencil"></span>
                </a>
                <form action="{{route('categories.destroy',[$categories->id])}}" method="POST"
                    onsubmit="return confirm('Move category to trash?')" class="d-inline">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm"><span class="fa fa-trash fa-lg"></span></button>
                </form>
            </div>
        </div>
        <div class="col-md-9">
            <div class="mt-4 mt-lg-2 mt-xl-2 mb-lg-1 mb-xl-1">
                {{-- Content --}}
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span style="width:100px;" class="input-group-text font-weight-bold"
                            id="inputGroup-sizing-default">Name</span>
                    </div>
                    <input type="text" class="form-control" aria-label="Default"
                        aria-describedby="inputGroup-sizing-default" value="{{$categories->name}}" disabled>
                </div>
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span style="width:100px;" class="input-group-text font-weight-bold"
                            id="inputGroup-sizing-default">Slug</span>
                    </div>
                    <input type="text" class="form-control" aria-label="Default"
                        aria-describedby="inputGroup-sizing-default" value="{{$categories->slug}}" disabled>
                </div>
                {{-- Content --}}
            </div>
        </div>
    </div>
</div>
@endsection
