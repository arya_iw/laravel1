@extends('layouts.app')
@section('script')
<script>
    $('.navbar-expand-md').addClass('fixed-top');
    $('body').css('background-color','rgba(128, 128, 128, 0.212)')
</script>
@endsection
@section('home-content')
<div class="jumbotron jumbotron-fluid bg-secondary mt-5">
    <div class="container">
        <div class="img-jumbotron text-center">
            <img src="/storage/{{ $categorie[0]['image'] }}" class="img-fluid rounded-circle bg-light" width="150px"
                height="100px" alt="">
            <h3 class="text-white font-weight-bold text-uppercase mt-2">{{ $categorie[0]['slug'] }}</h3>
        </div>
    </div>
</div>
<div class="row m-0 bg-white shadow-lg ">
    @foreach ($book as $item)
    <div class="col-lg-2 col-md-3 col-sm-4 col-6 pb-4 mt-3">
        <div class="card">
            <img src="/storage/{{$item->cover}}" alt="" class="img-thumbnail img-fluid">
            <div class="card-body">
                <p class="card-title text-truncate">{{$item->title}}</p>
                <p class="card-text">IDR {{number_format($item->price) }}</p>
            <a href="{{route('orders.show',[$item->id])}}" class="btn btn-primary btn-sm  btn-block text-white">Buy</a>
            </div>
        </div>
    </div>
    @endforeach
    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
    {{$book->appends(Request::all())->links()}}
    </div>
</div>
@endsection
