@extends('layouts.global2')
@section('title')
Category List
@endsection
@section('content')
@if(session('status'))
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-{{session('type')}}">
            {{session('status')}}
        </div>
    </div>
</div>
@endif
<div class="row mb-2">
    <div class="col-6">
        <form action="{{ route('categories.index') }} ">
            <div class="input-group">
                <input type="text" class="form-control" aria-describedby="basic-addon2" name="keyword"
                    value="{{Request::get('keyword')}}" placeholder="Filter Berdasarkan Nama Barang">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit">Filter</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-6">
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="nav-link active" href="{{route('categories.index')}}">Published</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('categories.trash')}}">Trash</a>
            </li>
        </ul>
    </div>
</div>
<hr class="my-3">
<div class="row overflow-x-y-75">
    <div class="col-md-12">
        <div class="row mb-3">
            <div class="col-md-12 text-right">
                <a href="{{route('categories.create')}}" class="btn btn-primary">Create Category</a>
            </div>
        </div>
        <div class="table-responsive-md">
            <table class="table table-bordered table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Slug</th>
                        <th scope="col">Image</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categories as $all_categories)
                    <tr>
                        {{-- <th scope="row">{{ $a++ }} </th> --}}
                        <th scope="row">{{ $all_categories->name }} </th>
                        <td>{{ $all_categories->slug }} </td>
                        <td>
                            @if($all_categories->image)
                            <img src="{{asset('storage/'.$all_categories->image)}}" width="70px" height="50px" />
                            @else
                            No Image
                            @endif
                        </td>
                        <td>
                            <a href="{{route('categories.edit',[$all_categories->id])}}"
                                class="btn btn-info btn-sm "><span class="oi oi-pencil"></span></a>
                            <a class="btn btn-sm btn-primary"
                                href="{{route('categories.show',[$all_categories->id])}}"><span
                                    class="fa fa-info-circle fa-lg"></span></a>
                            <form action="{{route('categories.destroy',[$all_categories->id])}}" method="POST"
                                onsubmit="return confirm('Move category to trash?')" class="d-inline">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class="btn btn-danger btn-sm"><span
                                        class="fa fa-trash fa-lg"></span></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=10>
                            {{$categories->appends(Request::all())->links()}}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

@endsection
