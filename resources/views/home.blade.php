@extends('layouts.global2')@section("title")Home
@endsection
@section('content')
<div class="row">
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Name</div>
                        <div class="h6 mb-0 font-weight-bold text-gray-800">{{Auth::user()->name}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-user fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Roles</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                            @if (Auth::user()->roles == '["ADMIN"]')
                            ADMIN
                            @elseif(Auth::user()->roles == '["STAFF"]')
                            STAFF
                            @elseif(Auth::user()->roles == '["CUSTOMER"]')
                            CUSTOMER
                            @endif
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-user-tag fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-dark shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">total users</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$all_users}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <a href="{{route('orders.personal')}}"
                            class="text-xs font-weight-bold text-primary text-uppercase mb-1 stretched-link">My
                            Orders</a>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$all_orders}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-2 bg-white">
        <div class="card">
            <div class="card-header">
            <a href="{{route('index')}}" class="btn btn-warning text-white">Create Order</a>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <div class="card bg-white shadow-lg rounded">
            <div class="card-header">Wellcome {{Auth::user()->name}}</div>
            <div class="card-body">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif
                Bookstore is a private website intended for book sales where users can buy and ask for availability of
                goods directly to the admin via the messages feature and can see the status of orders whether they have
                been processed or sent.
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="card shadow-lg rounded">
            <div class="card-header">Admin and staff contact</div>
            <div class="card-body overflow-x-y-auto">
                @foreach ($chat as $chats)
                <form action="{{route('messages.show',[$chats])}}">
                    <div class="media shadow-lg bg-white mt-3 p-3">
                        <img src="/storage/{{$chats->avatar}}" class="mr-3" alt="..." height="60px" height="60px">
                        <div class="media-body">
                            <p class="mt-0">{{$chats->name}}</p>
                            <button type="submit" class="btn btn-primary btn-sm">Chat</button>
                        </div>
                    </div>
                </form>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
