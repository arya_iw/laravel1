<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>
    <div class="container-fluid mb-5">
        <div class="row shadow-lg bg-white m-1 rounded p-1">
            <div class="col-md-12">
                @foreach ($transaction as $detail)
                <h1 class=" font-weight-bold">BookStore</h1>
                <h3>IDR {{ number_format($detail->total_price) }}</h3>
                <hr class=" border-dark">
                <h5 class="font-weight-bold">{{ $detail->pays->payment_type }}</h5>
                <p style="background-color: rgba(128, 128, 128, 0.452)" class="pl-3">{{ $detail->pays->updated_at }}</p>
                <div class="text-center text-uppercase bg-success text-white mt-n3">TRANSAKSI {{ $detail->pays->status }}</div>
                <div class="m-4">
                    <p>Dear {{ $detail->user->name }}</p>
                    <p>Transaksi Anda berhasil! Silakan lihat pesanan Anda di bawah ini</p>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Deskripsi</th>
                                    <th scope="col">Harga</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    @foreach ($detail->books as $books)
                                    <td class="w-75">{{$books->pivot->quantity}}x {{$books->title }}</td>
                                    @endforeach
                                    <td>IDR {{ number_format($detail->total_price - $detail->ship->cost)}}</td>
                                </tr>
                                <tr>
                                    <td class="text-capitalize w-75">ongkir {{$detail->ship->courier }}</td>
                                    <td>IDR {{number_format($detail->ship->cost)}}</td>
                                </tr>
                                <tr>
                                    <td class="w-75 font-weight-bold">TOTAL</td>
                                    <td class="font-weight-bold">IDR {{ number_format($detail->total_price) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
</body>

</html>
