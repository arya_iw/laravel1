@extends('layouts.global2')
<style>
    .table-responsive-md,
    .table {
        transform: rotateX(360deg) !important;
        -moz-transform: rotateX(360deg) !important;
        /* Mozilla */
        -webkit-transform: rotateX(360deg) !important;
        /* Safari and Chrome */
        -ms-transform: rotateX(360deg) !important;
        /* IE 9+ */
        -o-transform: rotateX(360deg) !important;
        /* Opera */
    }

    table.table-bordered>tbody>tr>td {
        border: 1px solid rgba(0, 0, 0, 0.452) !important;
    }
</style>
@section('title')
Print Report Orders Pdf
@endsection
@section('footer-scripts')

@endsection
@section('content')
<div class="row shadow-lg bg-white m-1 rounded p-1">
    <div class="col-md-12">
        <h1 class="font-weight-bold">BookStore Sales Report</h1>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Invoice Number</th>
                        <th scope="col">Item</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Total Price</th>
                        <th scope="col">Buyer</th>
                        <th scope="col">Order Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($export as $exports)
                    <tr>
                        <td>{{ $exports->invoice_number }}</td>
                        @foreach ($exports->books as $book)
                        <td>{{ $book->title }}</td>
                        @endforeach
                        <td>{{ $exports->totalQuantity }}</td>
                        <td>IDR {{ number_format($exports->total_price) }}</td>
                        <td>{{ $exports->user->name }}</td>
                        <td>{{ $exports->updated_at }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="3" class="font-weight-bold">Total Price of All Orders</td>
                        <td colspan="3">IDR {{ number_format($totalAllPrice) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12">
        <form action="{{ route('print.report.orders') }}" method="GET" enctype="multipart/form-data">
            <input type="date" name="end_date" value="{{ session('end_date') }}" hidden>
            <input type="date" name="initial_date" value="{{ session('initial_date') }}" hidden>
            <input type="submit" value="Export Pdf" class="btn btn-block btn-primary mb-2">
        </form>
    </div>
</div>
@endsection
