@extends('layouts.global2')
<style>
    .table-responsive-md,
    .table {
        transform: rotateX(360deg) !important;
        -moz-transform: rotateX(360deg) !important;
        /* Mozilla */
        -webkit-transform: rotateX(360deg) !important;
        /* Safari and Chrome */
        -ms-transform: rotateX(360deg) !important;
        /* IE 9+ */
        -o-transform: rotateX(360deg) !important;
        /* Opera */
    }
</style>
@section('title')
Print Transaction Pdf
@endsection
@section('footer-scripts')

@endsection
@section('content')
<div class="row shadow-lg bg-white m-1 rounded p-1">
    <div class="col-md-12">
        @foreach ($transaction as $detail)
        <h1 class=" font-weight-bold">BookStore</h1>
        <h3>IDR {{ number_format($detail->total_price) }}</h3>
        <hr class=" border-dark">
        <h5 class="font-weight-bold">{{ $detail->pays->payment_type }}</h5>
        <p style="background-color: rgba(128, 128, 128, 0.452)" class="pl-3">{{ $detail->pays->updated_at }}</p>
        <div class="text-center text-uppercase bg-success text-white mt-n3">TRANSAKSI {{ $detail->pays->status }}</div>
        <div class="m-4">
            <p>Dear {{ $detail->user->name }}</p>
            <p>Transaksi Anda berhasil! Silakan lihat pesanan Anda di bawah ini</p>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Deskripsi</th>
                            <th scope="col">Harga</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            @foreach ($detail->books as $books)
                            <td class="w-75">{{$books->pivot->quantity}}x {{$books->title }}</td>
                            @endforeach
                            <td>IDR {{ number_format($detail->total_price - $detail->ship->cost)}}</td>
                        </tr>
                        <tr>
                            <td class="text-capitalize w-75">ongkir {{$detail->ship->courier }}</td>
                            <td>IDR {{number_format($detail->ship->cost)}}</td>
                        </tr>
                        <tr>
                            <td class="w-75 font-weight-bold">TOTAL</td>
                            <td class="font-weight-bold">IDR {{ number_format($detail->total_price) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        @endforeach
    </div>
    <div class="col-md-12">
        <a href="{{ route('print.transaction',[$transaction[0]['id']])}}" class="btn btn-block btn-primary mb-2">Export Pdf</a>
    </div>
</div>
@endsection
