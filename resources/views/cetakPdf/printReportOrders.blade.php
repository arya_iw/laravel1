<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <style>
            table.table-bordered>tbody>tr>td {
        border: 1px solid rgba(0, 0, 0, 0.452) !important;
    }
        </style>
</head>

<body>
    <div class="container-fluid mb-5">
        <div class="row shadow-lg bg-white m-1 rounded p-1">
            <div class="col-md-12">
                <h1 class="font-weight-bold">BookStore Sales Report</h1>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Invoice Number</th>
                                <th scope="col">Item</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Total Price</th>
                                <th scope="col">Buyer</th>
                                <th scope="col">Order Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($export as $exports)
                            <tr>
                                <td>{{ $exports->invoice_number }}</td>
                                @foreach ($exports->books as $book)
                                <td>{{ $book->title }}</td>
                                @endforeach
                                <td>{{ $exports->totalQuantity }}</td>
                                <td>IDR {{ number_format($exports->total_price) }}</td>
                                <td>{{ $exports->user->name }}</td>
                                <td>{{ $exports->updated_at }}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="3" class="font-weight-bold">Total Price of All Orders</td>
                                <td colspan="3">IDR {{ number_format($export->sum('total_price')) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
</body>

</html>
