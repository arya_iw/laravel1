<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" class="form-data" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- isi --}}
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                @csrf
                                @method("POST")
                                <div class="form-group">
                                    <input type="hidden" name="id" id="id">
                                    <label for="name">Name</label>
                                    <input class="form-control" type="text" name="name" id="name">
                                </div>
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input class="form-control " type="username" name="username" id="username" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="current_password">Current Password</label>
                                    <input class="form-control" type="password" name="current_password" id="current_password">
                                </div>
                                <div class="form-group">
                                    <label for="email">New Password</label>
                                    <input class="form-control " type="password" name="password" id="password">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input class="form-control" type="text" name="email" id="email" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Phone Number</label>
                                    <input class="form-control" type="number" name="phone" id="phone">
                                </div>
                                <img class="img-thumbnail" src="/storage/{{Auth::user()->avatar}}" alt="" width="100px"
                                    height="100px">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="avatar" name="avatar">
                                    <label class="custom-file-label" for="avatar">Choose file Avatar</label>
                                </div>
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <textarea id="address" class="form-control" rows="3" name="address"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- isi --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
