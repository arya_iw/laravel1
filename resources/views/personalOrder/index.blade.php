<script
    src="{{ !config('services.midtrans.isProduction') ? 'https://app.sandbox.midtrans.com/snap/snap.js' : 'https://app.midtrans.com/snap/snap.js' }}"
    data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
@extends('layouts.global2')
@section('title')
My Orders
@endsection
@section('footer-scripts')
<script src="{{asset('/js/messages_contact.js')}}"></script>
<script>
    function paySubmit(id){
        $.post("{{ route('pay.submit') }}",
        {
            _method: 'POST',
            _token: '{{ csrf_token() }}',
            order_id : id,
        },
        function (data, status) {
            if(data == "stock"){
                $('.notif-stock').append(`
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="alert alert-warning mt-0" role="alert">
            <h4 class="alert-heading">less book stock</h4>
            <hr>
            <p>please contact admin to add book stock</p>
        </div>
                `);
            } else {
            snap.pay(data.snap_token, {
                // Optional
                onSuccess: function (result) {
                    location.reload();
                },
                // Optional
                onPending: function (result) {
                    location.reload();
                },
                // Optional
                onError: function (result) {
                    location.reload();
                }
            });
            }
        });
        return false;
    }
</script>
@endsection
@section('content')
<form action="{{route('orders.personal')}}">
    <div class="row notif-stock">
        <div class="col-md-12">
            @if (session('status'))
            <div class="alert alert-{{session('type')}}">
                {{session('status')}}
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <select name="status" class="form-control" id="status">
                    <option value="">ANY</option>
                    <option {{Request::get('status') == "SUBMIT" ? "selected" : ""}} value="SUBMIT">SUBMIT</option>
                    <option {{Request::get('status') == "PROCESS" ? "selected" : ""}} value="PROCESS">PROCESS</option>
                    <option {{Request::get('status') == "FINISH" ? "selected" : ""}} value="FINISH">FINISH</option>
                    <option {{Request::get('status') == "CANCEL" ? "selected" : ""}} value="CANCEL">CANCEL</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <input class="btn btn-primary" type="submit" value="Filter">
            <input type="button" value="Contact Owner" class="btn btn-primary" onclick="contactOwner()">
        </div>
    </div>
</form>
<div class="row">
    <div class="col-md-12 overflow-x-y-75">
        <hr class="my-3">
        <div class=" table-responsive-md">
            <table class="table table-bordered table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Invoice Number</th>
                        <th scope="col">Status Barang</th>
                        <th scope="col">Buyer</th>
                        <th scope="col">Total quantity</th>
                        <th scope="col">Order date</th>
                        <th scope="col">Total Price</th>
                        <th scope="col">Status Pembayaran</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                    <tr>
                        <td>{{$order->invoice_number}}</td>
                        <td>
                            @if ($order->status == "SUBMIT")
                            <span class="badge badge-warning text-light">{{$order->status}}</span>
                            @elseif($order->status == "PROCESS")
                            <span class="badge badge-info text-light">{{$order->status}}</span>
                            @elseif($order->status == "FINISH")
                            <span class="badge badge-success text-light">{{$order->status}}</span>
                            @elseif($order->status == "CANCEL")
                            <span class="badge badge-dark text-light">{{$order->status}}</span>
                            @endif
                        </td>
                        <td>
                            {{$order->user->name}} <br>
                            <small>{{$order->user->email}}</small>
                        </td>
                        <td>{{$order->totalQuantity}} pc (s)</td>
                        <td>{{$order->created_at}}</td>
                        <td>{{$order->total_price}}</td>
                        <td>
                            @if ($order->pays == "")
                            <button class="btn btn-sm btn-success" onclick="paySubmit({{ $order->id }})">Pay</button>
                            @else
                            @if($order->pays->status == "success")
                            <span class="badge badge-success text-light">{{$order->pays->status}}</span>
                            <br>
                            <a class="small text-primary" href="{{ route('transaction.details',[$order->id]) }}"><u>Export PDF</u></a>
                            @elseif(($order->pays->status == "pending") || ($order->pays->status == "submit"))
                            <button class="badge badge-warning text-light border-0"
                                onclick="snap.pay('{{ $order->pays->snap_token }}')">Complete Payment</button>
                            @elseif(($order->pays->status == "expired") || ($order->pays->status == "failure") ||
                            ($order->pays->status == "cancel") || ($order->pays->status == "failed"))
                            <span class="badge badge-danger text-light">{{$order->pays->status}}</span>
                            @endif
                            @endif
                        </td>
                        <td>
                            @if ($order->pays == "")
                                @if ($order->status == "SUBMIT")
                                <form onsubmit="return confirm('Cancel this Order?')" class="d-inline"
                                    action="{{route('orders.personal.update', [$order->id])}}" method="POST">
                                    @csrf
                                    @method("PUT")
                                    <button type="submit" class="btn btn-danger btn-sm">
                                        <i class="fas fa-lg fa-window-close"></i>
                                    </button>
                                </form>
                                <a class="btn btn-sm btn-primary mt-lg-auto mt-md-auto mt-sm-1 mt-xl-auto mb-lg-auto mb-xl-auto mb-md-1"
                                    href="{{ route('orders.personal.view',[$order->id])}} "><i
                                        class="fa fa-info-circle fa-lg"></i></a>
                                @endif
                            @else
                                @if ((($order->status == "SUBMIT") && ($order->pays->status == "submit"))||(($order->status
                                == "SUBMIT") && ($order->pays->status == "pending")))
                                <form onsubmit="return confirm('Cancel this Order?')" class="d-inline"
                                    action="{{route('orders.personal.update', [$order->id])}}" method="POST">
                                    @csrf
                                    @method("PUT")
                                    <button type="submit" class="btn btn-danger btn-sm">
                                        <i class="fas fa-lg fa-window-close"></i>
                                    </button>
                                </form>
                                <a class="btn btn-sm btn-primary mt-lg-auto mt-md-auto mt-sm-1 mt-xl-auto mb-lg-auto mb-xl-auto mb-md-1"
                                    href="{{ route('orders.personal.view',[$order->id])}} "><i
                                        class="fa fa-info-circle fa-lg"></i></a>
                                @else
                                <a class="btn btn-sm btn-primary mt-lg-auto mt-md-auto mt-sm-1 mt-xl-auto mb-lg-auto mb-xl-auto mb-md-1"
                                    href="{{ route('orders.personal.view',[$order->id])}} "><i
                                        class="fa fa-info-circle fa-lg"></i></a>
                                @endif
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">
                            {{$orders->appends(Request::all())->links()}}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@include('modal.messages_owner')
@endsection
