@extends('layouts.global2')
@section('title')
{{$orders->invoice_number}}
@endsection
@section('footer-scripts')

@endsection
@section('content')
<div class="row shadow-lg bg-white m-1 rounded p-1">
    <div class="col-md-6">
        {{--  --}}
        <div class="form-group">
            <label for="invoice_number">Invoice Number</label>
            <input class="form-control" value="{{$orders->invoice_number}}" type="text" name="invoice_number"
                id="invoice_number" disabled>
        </div>
        <div class="form-group">
            <label for="buyer">Pembeli</label>
            <input class="form-control" value="{{$orders->user->name}}" type="text" name="buyer" id="buyer" disabled>
        </div>
        <div class="form-group">
            <label for="created_at">Order Date</label>
            <input class="form-control" value="{{$orders->created_at}}" type="text" name="created_at" id="created_at"
                disabled>
        </div>
        <label for="">Books ({{$orders->totalQuantity}}) </label><br>
        <ul>
            @foreach($orders->books as $book)
            <li>{{$book->title}} <b>({{$book->pivot->quantity}})</b></li>
            @endforeach
        </ul>
        <div class="form-group">
            <label for="price">Price</label>
            <input class="form-control" value="{{$orders->total_price - $ship->cost}}" type="text" name="price" id="price" disabled>
        </div>
        <div class="form-group">
            <label for="price">Total Price</label>
            <input class="form-control" value="{{$orders->total_price}}" type="text" name="price" id="price"
                disabled>
        </div>
        <div class="form-group">
            <label for="status">Status</label>
            <input class="form-control" value="{{$orders->status}}" type="text" name="status" id="status" disabled>
        </div>
        {{--  --}}
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="origin_address">Origin Address</label>
            <textarea id="origin_address" class="form-control" rows="2" name="origin_address"
                disabled>{{$ship->origin_address}}</textarea>
        </div>
        <div class="form-group">
            <label for="destination_address">Destination Addres</label>
            <textarea id="destination_address" class="form-control" rows="2" name="destination_address"
                disabled>{{$ship->destination_address}}</textarea>
        </div>
        <div class="form-group">
            <label for="courier">Courier</label>
            <input class="form-control" type="text" name="courier" id="courier" value="{{$ship->courier}}" disabled>
        </div>
        <div class="form-group">
            <label for="cost">Cost</label>
            <input class="form-control" type="text" name="cost" id="cost" value="{{$ship->cost}}" disabled>
        </div>
    </div>
</div>
@endsection