@extends('layouts.app')
@section('script')
<script>
    $('.navbar-expand-md').addClass('fixed-top');
    $('.carousel-inner').children().first().addClass('active')
    $('.carousel-indicators').children().first().addClass('active')
    $('body').css('background-color','rgba(128, 128, 128, 0.212)')
</script>
@endsection
@section('home-content')
<div id="carouselExampleIndicators" class="carousel slide mt-5" data-ride="carousel">
    <ol class="carousel-indicators">
        @foreach ($slider as $sliders)
        <li data-target="#carouselExampleIndicators" data-slide-to="{{ $button_slider++ }}" class="orange"></li>
        @endforeach
    </ol>
    <div class="carousel-inner">
        @foreach ($slider as $sliders)
        <div class="carousel-item">
            <img class="d-block w-100 img-carousel" src="/storage/{{$sliders->img_slider}}" alt="First slide">
        </div>
        @endforeach
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<div class="row flex-nowrap overflow-auto mt-4">
    @foreach ($categorie as $categories)
    <div class="col-lg-2 col-md-2 col-sm-4 col-6">
        <div class="card bg-light rounded">
            <img src="/storage/{{ $categories->image }}" alt="" class="card-img img-fluid" height="200px">
            <div class="card-header text-center bg-light font-weight-bold">
                {{ $categories->slug }}
            </div>
            <a href="card/{{ $categories->slug }}/categorie" class="stretched-link"></a>
        </div>
    </div>
    @endforeach
</div>

<div class="row mt-5 m-0 p-0 bg-white shadow-lg">
    @foreach ($book as $item)
    <div class="col-lg-2 col-md-3 col-sm-4 col-6 pb-4 mt-3">
        <div class="card">
            <img src="/storage/{{$item->cover}}" alt="" class="img-thumbnail img-fluid">
            <div class="card-body">
                <p class="card-title text-truncate">{{$item->title}}</p>
                <p class="card-text">IDR {{number_format($item->price) }}</p>
            <a href="{{route('orders.show',[$item->id])}}" class="btn btn-primary btn-sm  btn-block text-white">Buy</a>
            </div>
        </div>
    </div>
    @endforeach
    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
    {{$book->appends(Request::all())->links()}}
    </div>
</div>
@endsection
