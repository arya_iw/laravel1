@extends('layouts.global2')
@section('title')
Edit User
@endsection
@section('content')
<div class="col-md-8">
    @if(session('status'))
    <div class="alert alert-success">
        {{session('status')}}
    </div>
    @endif
    <form enctype="multipart/form-data" class="bg-white shadow p-3" action="{{route('users.update', [$user->id])}}"
        method="POST">
        @csrf
        <input type="hidden" value="PUT" name="_method">
        <div class="form-group">
            <label for="name">Nama</label>
            <input class="form-control {{$errors->first('name') ? "is-invalid" : ""}}" type="text" name="name" id="name"
                placeholder="Fullname" value="{{old('name') ? old('name') : $user->name}}">
            <div class="invalid-feedback">
                {{$errors->first('name')}}
            </div>
        </div>
        <div class="form-group">
            <label for="username">Username</label>
            <input class="form-control" type="text" name="username" id="username" value="{{ $user->username }}"
                disabled>
            <small class="form-text text-muted">Name in your dashboard</small>
        </div>
        <label for="">Status</label><br>
        <div class="form-check form-check-inline ml-3">
            <input {{$user->status == "ACTIVE" ? "checked" : ""}} class="form-check-input" type="radio" name="status"
                id="active" value="ACTIVE">
            <label class="form-check-label" for="active">Active</label>
        </div>
        <div class="form-check form-check-inline">
            <input {{$user->status == "INACTIVE" ? "checked" : ""}} class="form-check-input" type="radio" name="status"
                id="inactive" value="INACTIVE">
            <label class="form-check-label" for="inactive">Inactive</label>
        </div>
        <br><label class="mt-3" for="">Roles</label><br>
        <div class=" form-check form-check-inline ml-3">
            <input {{in_array("ADMIN", json_decode($user->roles)) ? "checked" : ""}} class="form-check-input {{$errors->first('roles') ? "is-invalid" : ""}}"
                type="radio" value="ADMIN" name="roles[]" id="ADMIN">
            <label class="form-check-label" for="ADMIN">Administrator</label>
        </div>
        <div class="form-check form-check-inline">
            <input {{in_array("STAFF", json_decode($user->roles)) ? "checked" : ""}} class="form-check-input {{$errors->first('roles') ? "is-invalid" : ""}}"
                type="radio" value="STAFF" name="roles[]" id="STAFF">
            <label class="form-check-label" for="STAFF">Staff</label>
        </div>
        <div class="form-check form-check-inline">
            <input {{in_array("CUSTOMER", json_decode($user->roles)) ? "checked" : ""}} class="form-check-input {{$errors->first('roles') ? "is-invalid" : ""}}"
                type="radio" value="CUSTOMER" name="roles[]" id="CUSTOMER">
            <label class="form-check-label" for="CUSTOMER">Customer</label>
        </div>
        <div class="form-group mt-2">
            <label for="phone">Phone number</label>
            <input class="form-control {{$errors->first('phone') ? "is-invalid" : ""}}" type="text" name="phone" id="phone" value="{{old('phone') ? old('phone') : $user->phone}}">
            <small class="form-text text-muted">Nomer Handphone Anda</small>
            <div class="invalid-feedback">
                {{$errors->first('phone')}}
            </div>
        </div>
        <div class="form-group">
            <label for="address">Address</label>
            <textarea id="address" class="form-control {{$errors->first('address') ? "is-invalid" : ""}}" rows="2" name="address">{{old('address') ? old('address') : $user->address}}</textarea>
            <div class="invalid-feedback">
                {{$errors->first('address')}}
            </div>
        </div>
        @if ($user->avatar)
        <img src="{{asset('storage/'.$user->avatar)}}" width="120" /><br>
        <br>
        @else
        <label class="bg-danger text-white pl-1 pr-1" for="">No Avatar</label><br>
        @endif
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="avatar" name="avatar">
            <label class="custom-file-label" for="avatar">Avatar Image</label>
            <small class="form-text text-muted">Abaikan jika tidak ingin mengganti avatar</small>
        </div>
        <hr class="my-3 font-weight-bold">
        <div class="form-group">
            <label for="email">Email</label>
            <input class="form-control" type="email" name="email" id="email" placeholder="user@mail.com"
                value="{{ $user->email }}" disabled>
            <small class="form-text text-muted"></small>
        </div>
        <input class="btn btn-primary" type="submit" value="Save">
    </form>
</div>
@endsection
