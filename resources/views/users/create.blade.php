@extends("layouts.global2")
@section("title") Create User @endsection
@section("content")
<div class="row">
    <div class="col-md-8">
        @if(session('status'))
        <div class="alert alert-success">
            {{session('status')}}
        </div>
        @endif
        <form enctype="multipart/form-data" class="bg-white shadow-sm p-3" action="{{route('users.store')}}"
            method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Nama</label>
                <input class="form-control {{$errors->first('name') ? "is-invalid": ""}}" type="text" name="name"
                    id="name" placeholder="Fullname" value="{{old('name')}}">
                <div class="invalid-feedback">
                    {{$errors->first('name')}}
                </div>
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input class="form-control {{$errors->first('username') ? "is-invalid": ""}}" type="text"
                    name="username" id="username" value="{{old('username')}}">
                <div class="invalid-feedback">
                    {{$errors->first('username')}}
                </div>
                <small class="form-text text-muted">Name in your dashboard</small>
            </div>
            <label for="">Roles</label><br>
            <div class=" form-check form-check-inline ml-3">
                <input class="form-check-input {{$errors->first('roles') ? "is-invalid": ""}}" type="radio"
                    value="ADMIN" name="roles[]" id="ADMIN">
                <label class="form-check-label" for="ADMIN">Administrator</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input {{$errors->first('roles') ? "is-invalid": ""}}" type="radio"
                    value="STAFF" name="roles[]" id="STAFF">
                <label class="form-check-label" for="STAFF">Staff</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input {{$errors->first('roles') ? "is-invalid": ""}}" type="radio"
                    value="CUSTOMER" name="roles[]" id="CUSTOMER">
                <label class="form-check-label" for="CUSTOMER">Customer</label>
            </div>
            <div class="invalid-feedback">
                {{$errors->first('roles')}}
            </div>
            <br>
            <div class="form-group mt-2">
                <label for="phone">Phone number</label>
                <input class="form-control {{$errors->first('phone') ? "is-invalid" : ""}}" type="text" name="phone"
                    id="phone" value="{{old('phone')}}">
                <small class="form-text text-muted">Nomer Handphone Anda</small>
                <div class="invalid-feedback">
                    {{$errors->first('phone')}}
                </div>
            </div>
            <div class="form-group">
                <label for="address">Address</label>
                <textarea id="address" class="form-control {{$errors->first('address') ? "is-invalid" : ""}}" rows="2"
                    name="address">{{old('address')}}</textarea>
                <div class="invalid-feedback">
                    {{$errors->first('address')}}
                </div>
            </div>
            <div class="custom-file">
                <input type="file" class="custom-file-input {{$errors->first('avatar') ? "is-invalid" : ""}}"
                    id="avatar" name="avatar">
                <label class="custom-file-label" for="avatar">Avatar Image</label>
                <div class="invalid-feedback">
                    {{$errors->first('avatar')}}
                </div>
            </div>
            <hr class="my-3">
            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control {{$errors->first('email') ? "is-invalid" : ""}}" type="email" name="email"
                    id="email" placeholder="user@mail.com" value="{{old('email')}}">
                <small class="form-text text-muted"></small>
                <div class="invalid-feedback">
                    {{$errors->first('email')}}
                </div>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control {{$errors->first('password') ? "is-invalid" : ""}}" type="password"
                    name="password" id="password" placeholder="password">
                <small class="form-text text-muted"></small>
                <div class="invalid-feedback">
                    {{$errors->first('password')}}
                </div>
            </div>
            <div class="form-group">
                <label for="password_confirmation">Password Confirmation</label>
                <input class="form-control {{$errors->first('password_confirmation') ? "is-invalid" : ""}}"
                    type="password" name="password_confirmation" id="password_confirmation"
                    placeholder="password confirmation">
                <small class="form-text text-muted"></small>
                <div class="invalid-feedback">
                    {{$errors->first('password_confirmation')}}
                </div>
            </div>
            <input class="btn btn-primary" type="submit" value="Save">
        </form>
    </div>
</div>
@endsection
