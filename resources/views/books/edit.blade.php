@extends('layouts.global2')
@section('footer-scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-
rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-
rc.0/js/select2.min.js"></script>
<script>
    $('#categories').select2({
        ajax: {
            url: 'http://127.0.0.1:8000/ajax/categories/search',
            processResults: function(data){
                return {
                    results: data.map(function(item){return {id: item.id, text:item.name} })
            }
        }
    }
});
var categories = {!! $book->categories !!}
    categories.forEach(function(category){
        var option = new Option(category.name, category.id, true, true);
        $('#categories').append(option).trigger('change');
    });
</script>
@endsection
@section('title')
Edit Book
@endsection
@section('content')
<div class="row">
    <div class="col-md-8">
        @if(session('status'))
        <div class="alert alert-success">
            {{session('status')}}
        </div>
        @endif
        <form action="{{route('books.update',[$book->id])}}" method="post" class=" bg-white p-3 rounded shadow"
            enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group">
                <label for="title">Title</label>
                <input value="{{old('title') ? old('title') : $book->title}}"
                    class="form-control {{$errors->first('title') ? "is-invalid" : ""}}" type="text" name="title"
                    id="title" placeholder="Book Titile">
                <small class="form-text text-muted">Title your book</small>
                <div class="invalid-feedback">
                    {{$errors->first('title')}}
                </div>
            </div>
            <label for="cover">Current Image</label><br>
            @if ($book->cover)
            <img class="mb-2" src="{{asset('storage/'.$book->cover)}}" alt="" width="90px">
            @endif
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="cover" name="cover">
                <label class="custom-file-label" for="cover">Choose a cover image</label>
                <small class="text-muted">Kosongkan jika tidak ingin merubah cover</small>
            </div>
            <div class="form-group mt-2">
                <label for="slug">Slug</label>
                <input value="{{old('slug') ? old('slug') : $book->slug}}"
                    class="form-control {{$errors->first('slug') ? "is-invalid" : ""}}" type="text" name="slug"
                    id="slug" placeholder="Enter e slug">
                <div class="invalid-feedback">
                    {{$errors->first('slug')}}
                </div>
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea id="description" class="form-control {{$errors->first('description') ? "is-invalid" : ""}}"
                    rows="2"
                    name="description">{{old('description') ? old('description') : $book->description}}</textarea>
                <div class="invalid-feedback">
                    {{$errors->first('description')}}
                </div>
            </div>
            <div class="form-group">
                <label for="categories">Categories</label>
                <select class="form-control" id="categories" multiple name="categories[]">
                </select>
            </div>
            <div class="form-group">
                <label for="stock">Stock</label>
                <input value="{{old('stock') ? old('stock') : $book->stock}}"
                    class="form-control {{$errors->first('stock') ? "is-invalid" : ""}}" type="number" name="stock"
                    id="stock" min="0">
                <small class="form-text text-muted">Stock your book</small>
                <div class="invalid-feedback">
                    {{$errors->first('stock')}}
                </div>
            </div>
            <div class="form-group">
                <label for="author">Author</label>
                <input value="{{old('author') ? old('author') : $book->author}}"
                    class="form-control {{$errors->first('author') ? "is-invalid" : ""}}" type="text" name="author"
                    id="author">
                <small class="form-text text-muted">Author yout book</small>
                <div class="invalid-feedback">
                    {{$errors->first('author')}}
                </div>
            </div>
            <div class="form-group">
                <label for="publisher">Publisher</label>
                <input value="{{old('publisher') ? old('publisher') : $book->publisher}}"
                    class="form-control {{$errors->first('publisher') ? "is-invalid" : ""}}" type="text"
                    name="publisher" id="publisher">
                <small class="form-text text-muted">Publisher your book</small>
                <div class="invalid-feedback">
                    {{$errors->first('publisher')}}
                </div>
            </div>
            <div class="form-group">
                <label for="price">Price</label>
                <input value="{{old('price') ? old('price') : $book->price}}"
                    class="form-control {{$errors->first('price') ? "is-invalid" : ""}}" type="number" name="price"
                    id="price" min="0">
                <small class="form-text text-muted">Price your book</small>
                <div class="invalid-feedback">
                    {{$errors->first('price')}}
                </div>
            </div>
            <div class="form-group">
                <label for="status">Example select</label>
                <select class="form-control" id="status" name="status">
                    <option {{$book->status == 'PUBLISH' ? 'selected':''}} value="PUBLISH">PUBLISH</option>
                    <option {{$book->status == 'DRAFT' ? 'selected':''}} value="DRAFT">DRAFT</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection
