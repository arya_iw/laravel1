@extends('layouts.global2')
@section('title')
Trashed Book
@endsection
@section('content')
<div class="row">
    <div class="col-md-6 mb-2 mb-lg-auto mb-xl-auto mb-md-auto">
        <form action="{{route('books.index')}}">
            <div class="input-group">
                <input name="keyword" type="text" value="{{Request::get('keyword')}}" class="form-control"
                    placeholder="Filter by title">
                <div class="input-group-append">
                    <input type="submit" value="Filter" class="btn btn-primary">
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-6">
        <ul class="nav nav-pills float-right">
            <li class="nav-item">
                <a class="nav-link {{Request::get('status') == NULL && Request::path() == 'books' ? 'active' : ''}}"
                    href="{{route('books.index')}}">All</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Request::get('status') == 'publish' ? 'active' : '' }}"
                    href="{{route('books.index', ['status' => 'publish'])}}">Publish</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Request::get('status') == 'draft' ? 'active' : '' }}"
                    href="{{route('books.index', ['status' => 'draft'])}}">Draft</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Request::path() == 'books/trash' ? 'active' : ''}}"
                    href="{{route('books.trash')}}">Trash</a>
            </li>
        </ul>
    </div>
</div>
<hr class="my-3">
<div class="row">
    <div class="col-md-12">
        @if(session('status'))
        <div class="alert alert-{{session('type')}} ">
            {{session('status')}}
        </div>
        @endif
        <div class="row mb-3">
            <div class="col-md-12 text-right">
                <a href="{{route('books.create')}}" class="btn btn-primary">Create book</a>
            </div>
        </div>
        <div class=" table-responsive-md">
            <table class="table table-bordered table-hover">
                <thead class=" thead-dark">
                    <tr>
                        <th scope="col">Cover</th>
                        <th scope="col">Title</th>
                        <th scope="col">Author</th>
                        <th scope="col">Status</th>
                        <th scope="col">Categories</th>
                        <th scope="col">Stock</th>
                        <th scope="col">Price</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($book_trash as $all_book)
                    <tr>
                        @if($all_book->cover)
                        <td>
                            <img src="{{asset('storage/'.$all_book->cover)}}" alt="" width="96">
                        </td>
                        @endif
                        <td>{{$all_book->title}}</td>
                        <td>{{$all_book->author}}</td>
                        <td>
                            @if ($all_book->status == "DRAFT")
                            <span class="badge bg-dark text-white">{{$all_book->status}}</span>
                            @elseif($all_book->status == "PUBLISH")
                            <span class="badge bg-success text-white">{{$all_book->status}} </span>
                            @endif
                        </td>
                        <td>
                            <ul class="pl-2">
                                @foreach ($all_book->categories as $categories)
                                <li>{{$categories->name}}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td>{{$all_book->stock}}</td>
                        <td>{{$all_book->price}}</td>
                        <td>
                            <a class="btn btn-success btn-sm" href="{{route('books.restore',[$all_book->id])}}"><span
                                    class="fa fa-trash-restore fa-lg text-white"></span></a>
                            <form action="{{route('books.deletepermanent',[$all_book->id])}}" class=" d-inline" method="POST"
                                onsubmit="return confirm('Delete this Book permanently?')">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class="btn btn-sm btn-danger"><span
                                        class="fa fa-trash fa-lg"></span></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=10>
                            {{$book_trash->appends(Request::all())->links()}}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
