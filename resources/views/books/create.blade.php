@extends('layouts.global2')
@section('footer-scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $('#categories').select2({
    ajax:{
    url:'http://127.0.0.1:8000/ajax/categories/search',
    processResults: function(data){
    return {
    results: data.map(function(item){return{id:item.id,text:item.name}})
    }
    }
    }
    });
</script>
@endsection
@section('title')
Create Book
@endsection
@section('content')
<div class="row">
    <div class="col-md-8">
        @if(session('status'))
        <div class="alert alert-success">
            {{session('status')}}
        </div>
        @endif
        <form action="{{route('books.store')}}" method="POST" class="p-3 rounded shadow bg-white"
            enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input class="form-control {{$errors->first('title') ? "is-invalid" : ""}}" value="{{old('title')}}"
                    type="text" name="title" id="title" placeholder="Book Title">
                <small class="form-text text-muted">Title your book</small>
                <div class="invalid-feedback">
                    {{$errors->first('title')}}
                </div>
            </div>
            <div class="custom-file">
                <input type="file" class="custom-file-input {{$errors->first('cover') ? "is-invalid" : ""}}" id="cover"
                    name="cover">
                <label class="custom-file-label" for="cover">Choose a cover image</label>
                <div class="invalid-feedback">
                    {{$errors->first('cover')}}
                </div>
            </div>
            <div class="form-group mt-2">
                <label for="description">Description</label>
                <textarea id="description" class="form-control {{$errors->first('description') ? "is-invalid" : ""}}"
                    rows="2" name="description"
                    placeholder="Give a description about this book">{{old('description')}}</textarea>
                <div class="invalid-feedback">
                    {{$errors->first('description')}}
                </div>
            </div>
            <div class="form-group">
                <label for="categories">Categories</label>
                <select class="form-control" id="categories" multiple name="categories[]">
                </select>
            </div>
            <div class="form-group">
                <label for="stock">Stock</label>
                <input value="{{old('stock') ? old('stock') : "0"}}"
                    class="form-control {{$errors->first('stock') ? "is-invalid" : ""}} " type="number" name="stock"
                    id="stock" min="0">
                <small class="form-text text-muted">Stock your book</small>
                <div class="invalid-feedback">
                    {{$errors->first('stock')}}
                </div>
            </div>
            <div class="form-group">
                <label for="author">Author</label>
                <input value="{{old('author')}}" class="form-control {{$errors->first('author') ? "is-invalid" : ""}}"
                    type="text" name="author" id="author">
                <small class="form-text text-muted">Book author</small>
                <div class="invalid-feedback">
                    {{$errors->first('author')}}
                </div>
            </div>
            <div class="form-group">
                <label for="publisher">Publisher</label>
                <input value="{{old('publisher')}}"
                    class="form-control {{$errors->first('publisher') ? "is-invalid" : ""}}" type="text"
                    name="publisher" id="publisher">
                <small class="form-text text-muted">Book publisher</small>
                <div class="invalid-feedback">
                    {{$errors->first('publisher')}}
                </div>
            </div>
            <div class="form-group">
                <label for="price">Price</label>
                <input value="{{old('price') ? old('price') : "0" }}"
                    class="form-control {{$errors->first('price') ? "is-invalid" : ""}}" type="number" name="price"
                    id="price" min="0">
                <small class="form-text text-muted">Book price</small>
                <div class="invalid-feedback">
                    {{$errors->first('price')}}
                </div>
            </div>
            <button class="btn btn-primary" name="save_action" value="PUBLISH">Publish</button>
            <button class="btn btn-secondary" name="save_action" value="DRAFT">Save as draft</button>
        </form>
    </div>
</div>
@endsection
