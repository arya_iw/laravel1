@extends('layouts.app')
@section('script')
<script>
    $('.navbar-expand-md').addClass('fixed-top');
    $('body').css('background-color','rgba(128, 128, 128, 0.212)')
</script>
@endsection
@section('home-content')
<form action="{{ route('index') }}">
    <input type="text" class="form-control" name="keyword" hidden value="{{Request::get('keyword')}}">
    <div class="row mt-5 bg-white shadow-lg m-0 pt-2 pb-2">
        <div class="col-md-4">
            <div class="form-group">
                <select class="form-control" id="exampleFormControlSelect1" name="price">
                    <option value="">Price</option>
                    <option value="DESC">Expensive</option>
                    <option value="ASC">Cheap</option>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <input type="submit" class="btn btn-primary" value="Sort">
        </div>
    </div>
</form>

<div class="row m-0 p-0 bg-white ">
    @foreach ($book as $item)
    <div class="col-lg-2 col-md-3 col-sm-4 col-6 pb-4 mt-3">
        <div class="card">
            <img src="/storage/{{$item->cover}}" alt="" class="img-thumbnail img-fluid">
            <div class="card-body">
                <p class="card-title text-truncate">{{$item->title}}</p>
                <p class="card-text">IDR {{number_format($item->price) }}</p>
                <a href="{{route('orders.show',[$item->id])}}"
                    class="btn btn-primary btn-sm  btn-block text-white">Buy</a>
            </div>
        </div>
    </div>
    @endforeach
    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
        {{$book->appends(Request::all())->links()}}
    </div>
    @if ($book->count() <= 0) <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="alert alert-warning mt-5 ALERT mt-0" role="alert">
            <h4 class="alert-heading">Not Found!</h4>
            <hr>
            <p>The book you are looking for does not exist, please enter data related to the book,
                such as the name of the book, the name of the publisher, the price of the book etc.</p>
        </div>
</div>
@endif
</div>
@endsection
