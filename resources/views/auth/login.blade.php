@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="offset-md-4 offset-sm-1 col-md-4 col-sm-10">
            <div class="card bg-white shadow-lg rounded-login">
                <div class="card-header-circle rounded-circle mt-n5 mx-auto">
                    <div class="card-icon fas fa-sign-in-alt"></div>
                </div>
            <div class="card-body">
                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                    @csrf
                    <div class="form-row">
                        <div class="input-group col-md-12">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-transparent" id="email"><i class="far fa-lg fa-envelope"></i></span>
                            </div>
                            <input id="email" type="email"
                                class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                value="{{ old('email') }}" required placeholder="Email">
                            @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="input-group col-md-12 mt-4 mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-transparent" id="password"><i class="fas fa-lg fa-lock"></i></span>
                            </div>
                            <input id="password" type="password"
                                class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                                required placeholder="Password">
                            @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <input type="checkbox" id="remember" name="remember"
                                    {{ old('remember') ? 'checked' : '' }}> <label
                                    for="remember">{{ __('Remember Me') }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mb-0 text-center">
                        <div class="col-md-12">
                            <button type="submit" class="btn-block btn btn-primary">
                                {{ __('Login') }}
                            </button>
                            <br>
                            <p class="p-0 mb-n2">Dont have an account?
                                <a class="btn btn-link pl-0" href="{{ route('register') }}">
                                    {{ __('Register now') }}
                                </a>
                            </p>
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
