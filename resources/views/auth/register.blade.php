@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="offset-md-4 offset-sm-1 col-md-4 col-sm-10">
            <div class="card bg-white shadow-lg rounded-login">
                <div class="card-header-circle rounded-circle mt-n5 mx-auto">
                    <div class="card-icon fas fa-user-circle"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-row">
                            <div class="input-group col-md-12">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent" id="name"><i
                                            class="fas fa-lg fa-user"></i></span>
                                </div>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" required autocomplete="name" autofocus
                                    placeholder="Name">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="input-group col-md-12 mt-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent" id="username"><i
                                            class="fas fa-lg fa-user"></i></span>
                                </div>
                                <input id="username" type="text"
                                    class="form-control @error('username') is-invalid @enderror" name="username"
                                    value="{{ old('username') }}" required autocomplete="username" autofocus
                                    placeholder="username">
                                @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="input-group col-md-12 mt-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent" id="phone"><i
                                            class="fa-lg fas fa-phone-square-alt"></i></span>
                                </div>
                                <input id="phone" type="number"
                                    class="form-control @error('phone') is-invalid @enderror" name="phone"
                                    value="{{ old('phone') }}" required autocomplete="phone" autofocus
                                    placeholder="Phone Number" min="0">
                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="input-group col-md-12 mt-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent" id="address"><i
                                            class="fas fa-lg fa-map-marked"></i></span>
                                </div>
                                <textarea id="address" class="form-control @error('address') is-invalid @enderror"
                                    rows="2" name="address" placeholder="Full Address">{{ old('address') }}</textarea>
                                @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="input-group col-md-12 mt-4 mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent" id="email"><i
                                            class="fas fa-lg fa-envelope"></i></span>
                                </div>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email"
                                    placeholder="E-Mail Address">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="input-group col-md-12">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent" id="password"><i
                                            class="fas fa-lg fa-lock"></i></span>
                                </div>
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="new-password" placeholder="Password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="input-group col-md-12 mt-4 mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent" id="password-confirm"><i
                                            class="fas fa-lg fa-lock"></i></span>
                                </div>
                                <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password"
                                    placeholder="Password Confirmation">
                            </div>
                        </div>

                        <div class="form-group row mb-0 text-center">
                            <div class="col-md-12">
                                <button type="submit" class="btn-block btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                                <br>
                                <p class="p-0 mb-n2">Already have an account?
                                    <a class="btn btn-link pl-0" href="{{ route('login') }}">
                                        {{ __('Log-in') }}
                                    </a>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
