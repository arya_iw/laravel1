@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="offset-md-4 offset-sm-1 col-md-4 col-sm-10">
            <div class="card bg-white shadow-lg rounded-login">
                <div class="card-header-circle rounded-circle mt-n5 mx-auto">
                    <div class="card-icon fas fa-key"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-row">
                            <div class="input-group col-md-12">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent" id="email"><i class="far fa-lg fa-envelope"></i></span>
                                </div>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="input-group col-md-12 mt-4 mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent" id="password"><i class="fas fa-lg fa-lock"></i></span>
                                </div>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="New Password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="input-group col-md-12 mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent" id="password-confirm"><i class="fas fa-lg fas fa-lock"></i></span>
                                </div>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Password Confirmation">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn-block btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
