@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="offset-md-4 offset-sm-1 col-md-4 col-sm-10">
            <div class="card bg-white shadow-lg rounded-login">
                    <div class="card-header-circle rounded-circle mt-n5 mx-auto">
                        <div class="card-icon fas fa-key"></div>
                    </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-row">
                            <div class="input-group col-md-12 mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent" id="email"><i class="far fa-lg fa-envelope"></i></span>
                                </div>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                                <br>
                                <p class="p-0 mb-n2 text-center">Dont have an account?
                                    <a class="btn btn-link pl-0" href="{{ route('register') }}">
                                        {{ __('Register now') }}
                                    </a>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
