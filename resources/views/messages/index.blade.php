@extends('layouts.global2')
@section('title')
Messages
@endsection
@section('footer-scripts')
<script>
    $('body').addClass('flow-x');
</script>
@endsection
@section('content')
<form action="{{route('messages.index')}} ">
    <div class="row mt-n4 mb-n2">
        <div class="col-5">
            <div class="form-group">
                <input class="form-control ml-1" type="text" name="filter" value="{{Request::get('filter')}}"
                    placeholder="Masukan name / email untuk filter">
            </div>
        </div>
        <div class="col-7">
            <div class="form-check form-check-inline">
                <input {{json_encode(Request::get('roles')) == '["ADMIN"]' ?'checked':''}} class="form-check-input"
                    type="radio" name="roles[]" id="ADMIN" value="ADMIN">
                <label class="form-check-label" for="ADMIN">ADMIN</label>
            </div>
            <div class="form-check form-check-inline">
                <input {{json_encode(Request::get('roles')) == '["STAFF"]' ?'checked':''}} class="form-check-input"
                    type="radio" name="roles[]" id="STAFF" value="STAFF">
                <label class="form-check-label" for="STAFF">STAFF</label>
                <button type="submit" class="btn btn-primary ml-1">filter</button>
            </div>
        </div>
    </div>
</form>
<div class="row m-1 overflow-x-y-80">
    <div class="col-lg-8 col-md-12 col-sm-12 col-12">
        @foreach ($chat as $chats)
        <form action="{{route('messages.show',[$chats])}}">
            <div class="media shadow-lg bg-white mt-3 p-3">
                <img src="/storage/{{$chats->avatar}}" class="mr-3" alt="..." height="60px" height="60px">
                <div class="media-body">
                    <p class="mt-0">{{$chats->name}}</p>
                    <button type="submit" class="btn btn-primary btn-sm">Chat</button>
                </div>
            </div>
        </form>
        @endforeach
    </div>
    <div class="col-lg-8 col-md-12 col-sm-12 col-12 mt-3">
        {{$chat->appends(Request::all())->links()}}
    </div>
</div>
@endsection