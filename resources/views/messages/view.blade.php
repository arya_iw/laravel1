@extends('layouts.global2')
@section('footer-scripts')
<script>
    $('body').addClass('flow-x');
    var element = document.getElementById("message");
        element.scrollTop = element.scrollHeight;
</script>
@endsection
@section('content')
<div class="row overflow-x-y-75" id="message">
    @foreach ($messages as $message)
    @if ($message->send == \Auth::user()->id)
    @if (isset($message->message) && isset($message->img_message))
    <div class="offset-lg-6 offset-md-6 offset-sm-6 col-lg-6 col-md-6 col-sm-6 col-12">
        <div class="card bg-transparent border-0 mb-3">
            <h5 class="card-header p-0 m-0 bg-transparent">
                <p class="small">
                    <img src="/storage/{{Auth::user()->avatar}}" class="mb-n2 rounded-circle" height="35px"
                        height="35px">
                    {{Auth::user()->name}}
                </p>
            </h5>
            <div class="card-body alert-warning border-top p-2 position-relative">
                {{--  --}}
                <form onsubmit="return confirm('Delete this user permanently?')" class="d-inline position-absolute"
                    style="right: 0 !important;" action="{{route('messages.destroy', [$message->id])}}" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="btn-group dropleft">
                        <button type="button" class="btn pl-0 box-shadow-none outline-none box-shadow-none"
                            data-toggle="dropdown">
                            <i class="fas fa-ellipsis-v"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-left mt-n1 border-0 bg-transparent">
                            <button type="submit" class=" border-0 bg-transparent outline-none"><i
                                    class="far fa-trash-alt text-danger fa-lg"></i></button>
                        </div>
                    </div>
                </form>
                {{--  --}}
                <p class="card-text">{{$message->message}}</p>
            </div>
        </div>
        <img src="/storage/{{$message->img_message}}" class="img-fluid mb-3">
    </div>
    @elseif(isset($message->message) && empty($message->img_message))
    <div class="offset-lg-6 offset-md-6 offset-sm-6 col-lg-6 col-md-6 col-sm-6 col-12">
        <div class="card bg-transparent border-0 mb-3">
            <h5 class="card-header p-0 m-0 bg-transparent">
                <p class="small">
                    <img src="/storage/{{Auth::user()->avatar}}" class="mb-n2 rounded-circle" height="35px"
                        height="35px">
                    {{Auth::user()->name}}
                </p>
            </h5>
            <div class="card-body alert-warning border-top p-2 position-relative">
                {{--  --}}
                <form onsubmit="return confirm('Delete this user permanently?')" class="d-inline position-absolute"
                    style="right: 0 !important;" action="{{route('messages.destroy', [$message->id])}}" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="btn-group dropleft">
                        <button type="button" class="btn pl-0 box-shadow-none outline-none box-shadow-none"
                            data-toggle="dropdown">
                            <i class="fas fa-ellipsis-v"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-left mt-n1 border-0 bg-transparent">
                            <button type="submit" class=" border-0 bg-transparent outline-none"><i
                                    class="far fa-trash-alt text-danger fa-lg"></i></button>
                        </div>
                    </div>
                </form>
                {{--  --}}
                <p class="card-text">{{$message->message}}
                </p>
            </div>
        </div>
    </div>
    @elseif(isset($message->img_message) && empty($message->message))
    <div class="offset-lg-6 offset-md-6 offset-sm-6 col-lg-6 col-md-6 col-sm-6 col-12">
        <h5 class="card-header p-0 m-0 bg-transparent border-0">
            <p class="small">
                <img src="/storage/{{Auth::user()->avatar}}" class="mb-n2 rounded-circle" height="35px" height="35px">
                {{Auth::user()->name}}
            </p>
        </h5>
        <img src="/storage/{{$message->img_message}}" class="img-fluid mb-3">
        <form onsubmit="return confirm('Delete this user permanently?')" class="d-inline"
            action="{{route('messages.destroy', [$message->id])}}" method="POST">
            @csrf
            <input type="hidden" name="_method" value="DELETE">
            <div class="btn-group dropright">
                <button type="button" class="btn dropdown-toggle pl-0 box-shadow-none outline-none box-shadow-none"
                    data-toggle="dropdown">
                    <i class="fas fa-ellipsis-v"></i>
                </button>
                <div class="dropdown-menu mt-n1 border-0 bg-transparent">
                    <button type="submit" class=" border-0 bg-transparent outline-none"><i
                            class="far fa-trash-alt text-danger fa-lg"></i></button>
                </div>
            </div>
        </form>
    </div>
    @endif
    @else
    @foreach ($chat_atribut as $chat)
    @if (isset($message->message) && isset($message->img_message))
    <div class="col-lg-6 col-md-6 col-sm-6 col-12 mr-1 ml-0">
        <div class="card bg-transparent border-0 mb-3">
            <h5 class="card-header p-0 m-0 bg-transparent">
                <p class="small">
                    <img src="/storage/{{$chat->avatar}}" class="mb-n2 rounded-circle" height="35px" height="35px">
                    {{$chat->name}}
                </p>
            </h5>
            <div class="card-body alert-dark border-top border-warning p-2">
                <p class="card-text">{{$message->message}}</p>
            </div>
        </div>
        <img src="/storage/{{$message->img_message}}" class="img-fluid mb-3">
    </div>
    @elseif(isset($message->message) && empty($message->img_message))
    <div class="col-lg-6 col-md-6 col-sm-6 col-12 mr-1 ml-0">
        <div class="card bg-transparent border-0 mb-3">
            <h5 class="card-header p-0 m-0 bg-transparent">
                <p class="small">
                    <img src="/storage/{{$chat->avatar}}" class="mb-n2 rounded-circle" height="35px" height="35px">
                    {{$chat->name}}
                </p>
            </h5>
            <div class="card-body alert-dark border-top border-warning p-2">
                <p class="card-text">{{$message->message}}</p>
            </div>
        </div>
    </div>
    @elseif(isset($message->img_message) && empty($message->message))
    <div class="col-lg-6 col-md-6 col-sm-6 col-12 mr-1 ml-0">
        <h5 class="card-header p-0 m-0 bg-transparent border-0">
            <p class="small">
                <img src="/storage/{{$chat->avatar}}" class="mb-n2 rounded-circle" height="35px" height="35px">
                {{$chat->name}}
            </p>
        </h5>
        <div class="img-message ">
            <img src="/storage/{{$message->img_message}}" class="img-fluid mb-3">
        </div>
    </div>
    @endif
    @endforeach
    @endif
    @endforeach
</div>
<form action="{{route('messages.store')}}" enctype="multipart/form-data" method="post">
    <div class="row bg-dark fixed-bottom footer-comment">
        @csrf
        <input type="hidden" name="sender_id" value="{{$id}}">
        <div class="col-lg-6 col-md-6 col-sm-6 col-6">
            <div class="custom-file mt-2 mb-2">
                <input type="file" class="custom-file-input @error("img_message") is-invalid @enderror" id="customFile"
                    name="img_message">
                <label class="custom-file-label" for="customFile">Choose file img comment</label>
                @error('img_message')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
                @enderror
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-6">
            <div class="input-group mt-2 mb-2 ml-n3">
                <input type="text" class="form-control @error("message") is-invalid @enderror"
                    placeholder="Recipient's message" name="message">
                <div class="input-group-append">
                    <button class="btn btn-outline-warning" type="submit">Send</button>
                </div>
                @error('message')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
                @enderror
            </div>
        </div>
    </div>
</form>
@endsection
