<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('auth.login');
// });
Auth::routes();

Route::get('/dahsboard', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/dahsboard', 'HomeController@index')->name('home');

// Router web larashop
Route::resource('users', 'UsersController');
//route Categories
Route::get('categories/trash', 'CategoryController@trash')->name('categories.trash');
Route::get('categories/{id}/restore', 'CategoryController@restore')->name('categories.restore');
Route::delete('categories/{id}/delete-permanent', 'CategoryController@deletepermanent')->name('categories.deletepermanent');
Route::resource('categories', 'CategoryController');
//Route Books
Route::delete('books/{id}/delete-permanent','BookController@deletepermanent')->name('books.deletepermanent');
Route::get('books/trash', 'BookController@trash')->name('books.trash');
Route::get('books/{id}/restore', 'BookController@restore')->name('books.restore');
Route::resource('books', 'BookController');
//route data ajax categories
Route::get('ajax/categories/search', 'CategoryController@ajaxSearch');
//route Order
Route::get('orders/personal','OrderController@personalOrder')->name('orders.personal');
Route::put('orders/personal/{order}', 'OrderController@personalOrderUpdate')->name('orders.personal.update');
Route::get('orders/personal/{order}', 'OrderController@personalOrderView')->name('orders.personal.view');
Route::resource('orders', 'OrderController');
//route data ajax book
Route::get('ajax/books/search','BookController@ajaxbooksearch')->name('books.data');
//pagination live serach sorting page orders
Route::get('fetch_book_data','OrderController@fetch_book_data');
Route::get('/province/{id}/cities','OrderController@getCities');
Route::get('/ongkir','OrderController@ongkir')->name('order.ongkir');
//route profile controller
Route::resource('/profile', 'ProfileController')->middleware('auth');
Route::get('/messages/owner','MessageController@contactOwner');
Route::resource('/messages','MessageController');

//route pay controller
Route::post('pay/submit','PayController@paySubmit')->name('pay.submit')->middleware('auth');
Route::post('pay/notif','PayController@payNotif');
Route::post('pay/finish',function(){
    return redirect()->route('orders.personal');
});

//route exportPdf
Route::get('transaction/{id}/details','exportPdf@show')->name('transaction.details')->middleware('auth');
Route::get('cetak/{id}/transaction','exportPdf@cetak')->name('print.transaction')->middleware('auth');
Route::post('reportorders','exportPdf@showReportOrderPdf')->name('report.orders')->middleware('auth');
Route::get('printreportorders','exportPdf@printReportOrderPdf')->name('print.report.orders')->middleware('auth');

//route setting web
Route::get('/','SettingController@sliderHome')->name('index');
Route::get('slider','SettingController@sliderIndex')->name('slider.index');
Route::delete('slider/{id}/delete','SettingController@sliderDestroy')->name('slider.delete');
Route::get('slider/create','SettingController@sliderCreate')->name('slider.create');
Route::post('slider/store','SettingController@sliderStore')->name('slider.store');
Route::get('card/{slug}/categorie','SettingController@cardCategorie')->name('card.categorie');
